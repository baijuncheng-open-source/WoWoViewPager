# WoWoViewPager



[![Download](![svg](https://images.gitee.com/uploads/images/2021/0607/094943_0fd5a4f7_8230582.gif "svg.gif"))](https://bintray.com/nightonke/maven/wowo-viewpager)
[![Lisense](https://img.shields.io/badge/License-Apache%202-lightgrey.svg)](https://www.apache.org/licenses/LICENSE-2.0)

![welcomePage1](https://images.gitee.com/uploads/images/2021/0607/094503_14b06687_8230582.gif "wowo_example1.gif")
 ![welcomePage2](https://images.gitee.com/uploads/images/2021/0607/095712_65310ded_8230582.gif "wowo_example2.gif")

WoWoViewPager combines ViewPager and Animations to provide a simple way to create applications' guide pages. When users are dragging WoWoViewPager, they become the director of the applications. The above gifs show how WoWoViewPager looks like, it supports some simple animations like translation, scale, alpha, background color animations, and moreover, some complicate animations like gif-playing, svg-drawing and path-drawing animations with corresponding custom views.

## Note
1. Here comes the 1.0.0 version， The new version provides plenty features for convenient usage and efficiency improvement. 

## Gradle 
```
 compile project(path: ':wowoviewpager')
```
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    testImplementation 'junit:junit:4.13'
    ohosTestImplementation 'com.huawei.ohos.testkit:runner:1.0.0.100'
    compile project(path: ':wowoviewpager')
}
```

## Maven

```
add library dependency at your module build.gradle
```

```
The lib is available on Maven Central, you can find it with [Gradle, please]

dependencies {
    implementation ‘com.gitee.baijuncheng-open-source：wowoviewpager：1.0.1’
}
```



# Demo

![main](https://images.gitee.com/uploads/images/2021/0607/100247_770d0d57_8230582.gif "main.gif")
Or by link:  

[WoWoViewPager V1.0.0 in Gitee](https://gitee.com/baijuncheng-open-source/WoWoViewPager/commit/97443bc3a65b5e45c66bb67894abe4f9bf5c329e?raw=true)  



### Documentation Chapters
#### [Basic Usage](https://github.com/Nightonke/WoWoViewPager/wiki/Basic-Usage)
#### [Ease](https://github.com/Nightonke/WoWoViewPager/wiki/Ease)
#### [Chameleon](https://github.com/Nightonke/WoWoViewPager/wiki/Chameleon)
#### [Typewriter](https://github.com/Nightonke/WoWoViewPager/wiki/Typewriter)
#### Basic Animations
1. [Position Animation](https://github.com/Nightonke/WoWoViewPager/wiki/Position-Animation)
1. [Position 3D Animation](https://github.com/Nightonke/WoWoViewPager/wiki/Position-3D-Animation)
1. [Translation Animation](https://github.com/Nightonke/WoWoViewPager/wiki/Translation-Animation)
1. [Translation 3D Animation](https://github.com/Nightonke/WoWoViewPager/wiki/Translation-3D-Animation)
1. [Scale Animation](https://github.com/Nightonke/WoWoViewPager/wiki/Scale-Animation)
1. [Alpha Animation](https://github.com/Nightonke/WoWoViewPager/wiki/Alpha-Animation)
1. [Rotation Animation](https://github.com/Nightonke/WoWoViewPager/wiki/Rotation-Animation)
1. [Elevation Animation](https://github.com/Nightonke/WoWoViewPager/wiki/Elevation-Animation)

#### TextView Animations
1. [TextView TextSize Animation](https://github.com/Nightonke/WoWoViewPager/wiki/TextView-TextSize-Animation)
1. [TextView TextColor Animation](https://github.com/Nightonke/WoWoViewPager/wiki/TextView-TextColor-Animation)
1. [TextView Text Animation](https://github.com/Nightonke/WoWoViewPager/wiki/TextView-Text-Animation)

#### Color Animations
1. [Background Color Animation](https://github.com/Nightonke/WoWoViewPager/wiki/Background-Color-Animation)
1. [Shape Color Animation](https://github.com/Nightonke/WoWoViewPager/wiki/Shape-Color-Animation)
1. [State-List Color Animation](https://github.com/Nightonke/WoWoViewPager/wiki/State-List-Color-Animation)
1. [Layer-List Color Animation](https://github.com/Nightonke/WoWoViewPager/wiki/Layer-List-Color-Animation)

#### [Path Animation](https://github.com/Nightonke/WoWoViewPager/wiki/Path-Animation)

#### [WoWoViewPager Attributes](https://github.com/Nightonke/WoWoViewPager/wiki/WoWoViewPager-Attributes)

#### Interface Expansibility
1. [Custom Animation](https://github.com/Nightonke/WoWoViewPager/wiki/Custom-Animation)
1. [Interface Animation](https://github.com/Nightonke/WoWoViewPager/wiki/Interface-Animation)
1. [SVG Animation](https://github.com/Nightonke/WoWoViewPager/wiki/SVG-Animation)
1. [GIF Animation](https://github.com/Nightonke/WoWoViewPager/wiki/GIF-Animation)

#### [Version History](https://github.com/Nightonke/WoWoViewPager/wiki/Version-History)
#### [How WoWoViewPager Works](https://github.com/Nightonke/WoWoViewPager/wiki/How-WoWoViewPager-Works)

## Issues & Feedbacks
Try to tell me the bugs or enhancements about WoWoViewPager, Before doing that, having a careful read on [read-me](https://gitee.com/baijuncheng-open-source/WoWoViewPager/blob/master/README.md) is really helpful.

## About Versions
Version 1.0.0 builder-pattern was used in WoWoViewPager and gif-playing, svg-drawing animations and more features are supported.