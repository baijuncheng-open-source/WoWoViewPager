package com.nightonke.wowoviewpagerexample;

import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;

public class AutoScrollActivity extends GuidePageActivity1 {


    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        DisplayAttributes attributes = DisplayManager.getInstance().getDefaultDisplay(getContext()).get().getRealAttributes();
        DependentLayout base = (DependentLayout) findComponentById(ResourceTable.Id_base);
        Button startButton = new Button(this);
        DependentLayout.LayoutConfig layoutParams = new DependentLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                ComponentContainer.LayoutConfig.MATCH_CONTENT);
        layoutParams.addRule(DependentLayout.LayoutConfig.ALIGN_PARENT_BOTTOM);
        layoutParams.addRule(DependentLayout.LayoutConfig.ALIGN_PARENT_RIGHT);
        startButton.setLayoutConfig(layoutParams);
        startButton.setText("Start Auto Scroll");
        startButton.setWidth(attributes.width);
        startButton.setPadding(0, 20, 0, 20);
        startButton.setMarginBottom(160);
        startButton.setTextSize(30);
        ShapeElement shapeElement = new ShapeElement(getContext(), ResourceTable.Graphic_background_white_rec);
        startButton.setBackground(shapeElement);
        startButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                wowo.startAutoScroll(false, 2000, 2000);
            }
        });
        base.addComponent(startButton);
        Button stopButton = new Button(this);
        DependentLayout.LayoutConfig layoutParams3 = new DependentLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                ComponentContainer.LayoutConfig.MATCH_CONTENT);
        layoutParams3.addRule(DependentLayout.LayoutConfig.ALIGN_PARENT_BOTTOM);
        layoutParams3.addRule(DependentLayout.LayoutConfig.ALIGN_PARENT_LEFT);
        stopButton.setLayoutConfig(layoutParams3);
        stopButton.setText("Stop Auto Scroll");
        stopButton.setMarginBottom(40);
        stopButton.setPadding(0, 20, 0, 20);
        stopButton.setWidth(attributes.width);
        stopButton.setTextSize(30);
        ShapeElement shapeElement2 = new ShapeElement(getContext(), ResourceTable.Graphic_background_white_rec);
        stopButton.setBackground(shapeElement2);
        stopButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                wowo.stopAutoScroll();
            }
        });
        base.addComponent(stopButton);

    }
}