package com.nightonke.wowoviewpagerexample;


import com.nightonke.wowoviewpager.Animation.PageAnimation;
import com.nightonke.wowoviewpager.Enum.TimeInterpolator;
import ohos.agp.components.Component;


/**
 * Created by Weiping Huang at 13:13 on 2017/4/2
 * For Personal Open Source
 * Contact me at 2584541288@qq.com or nightonke@outlook.com
 * For more projects: https://github.com/Nightonke
 */

public class CustomAnimation extends PageAnimation {

    private float fromElevation;
    private float toElevation;

    private CustomAnimation(int page, float startOffset, float endOffset, int ease, TimeInterpolator interpolator, boolean useSameEaseEnumBack, float fromElevation, float toElevation) {
        super(page, startOffset, endOffset, ease, interpolator, useSameEaseEnumBack);
        this.fromElevation = fromElevation;
        this.toElevation = toElevation;
    }

    @Override
    protected void toStartState(Component view) {
//            view.setElevation(fromElevation);
    }

    @Override
    protected void toMiddleState(Component view, float offset) {
//            view.setElevation(fromElevation + (toElevation - fromElevation) * offset);
    }

    @Override
    protected void toEndState(Component view) {
//            view.setElevation(toElevation);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder extends PageAnimation.Builder<Builder> {

        private float fromElevation = UNINITIALIZED_VALUE;
        private float toElevation = UNINITIALIZED_VALUE;

        public Builder from(float fromElevation) {
            this.fromElevation = fromElevation;
            return this;
        }

        public Builder to(float toElevation) {
            this.toElevation = toElevation;
            return this;
        }

        public Builder from(double fromElevation) {
            return from((float) fromElevation);
        }

        public Builder to(double toElevation) {
            return to((float) toElevation);
        }

        public CustomAnimation build() {
            checkUninitializedAttributes();
            return new CustomAnimation(page, startOffset, endOffset, ease, interpolator, useSameEaseEnumBack, fromElevation, toElevation);
        }

        @Override
        protected void checkUninitializedAttributes() {
            // Check the uninitialized attributes here if needed.
            if (fromElevation == UNINITIALIZED_VALUE) uninitializedAttributeException("fromElevation");
            if (toElevation == UNINITIALIZED_VALUE) uninitializedAttributeException("toElevation");
        }
    }
}
