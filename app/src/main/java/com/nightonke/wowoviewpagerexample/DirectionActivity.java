package com.nightonke.wowoviewpagerexample;


import com.nightonke.wowoviewpager.WoWoViewPager;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.element.ShapeElement;

public class DirectionActivity extends GuidePageActivity1 {


    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        DependentLayout base = (DependentLayout) findComponentById(ResourceTable.Id_base);

        Button directionButton = new Button(this);
        DependentLayout.LayoutConfig layoutParams3 = new DependentLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT,
                ComponentContainer.LayoutConfig.MATCH_CONTENT);
        layoutParams3.setMargins(0, 0, 20, 100);
        layoutParams3.addRule(DependentLayout.LayoutConfig.ALIGN_PARENT_BOTTOM);
        layoutParams3.addRule(DependentLayout.LayoutConfig.ALIGN_PARENT_RIGHT);
        directionButton.setLayoutConfig(layoutParams3);
        directionButton.setText("Change Direction");
        directionButton.setPadding(40, 20, 40, 20);
        directionButton.setMarginBottom(40);
        directionButton.setTextSize(30);
        ShapeElement shapeElement2 = new ShapeElement(getContext(), ResourceTable.Graphic_background_white_rec);
        directionButton.setBackground(shapeElement2);

        directionButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (wowo.getDirection() == WoWoViewPager.Horizontal) {
                    wowo.setDirection(WoWoViewPager.Vertical);
                } else {
                    wowo.setDirection(WoWoViewPager.Horizontal);
                }
            }
        });
        base.addComponent(directionButton);

    }
}
