package com.nightonke.wowoviewpagerexample;


import com.nightonke.wowoviewpager.Enum.WoWoGearbox;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.Slider;

public class GearboxActivity extends GuidePageActivity1 {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        DependentLayout base = (DependentLayout) findComponentById(ResourceTable.Id_base);

        Slider seekBar = new Slider(this);
        DependentLayout.LayoutConfig layoutParams = new DependentLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        layoutParams.setMargins(0, 0, 0, 20);
        layoutParams.addRule(DependentLayout.LayoutConfig.ALIGN_PARENT_BOTTOM);
        seekBar.setLayoutConfig(layoutParams);

        seekBar.setMaxValue(WoWoGearbox.Gearboxes.length - 1);
        seekBar.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean fromUser) {
                if (fromUser) wowo.setGearbox(WoWoGearbox.Gearboxes[progress]);
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });

        seekBar.setProgressValue(3);
        base.addComponent(seekBar);


    }
}
