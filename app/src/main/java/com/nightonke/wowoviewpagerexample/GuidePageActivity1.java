package com.nightonke.wowoviewpagerexample;


import com.nightonke.wowoviewpager.Animation.WoWoAlphaAnimation;
import com.nightonke.wowoviewpager.Animation.WoWoPathAnimation;
import com.nightonke.wowoviewpager.Animation.WoWoRotationAnimation;
import com.nightonke.wowoviewpager.Animation.WoWoScaleAnimation;
import com.nightonke.wowoviewpager.Animation.WoWoShapeColorAnimation;
import com.nightonke.wowoviewpager.Animation.WoWoTextViewTextAnimation;
import com.nightonke.wowoviewpager.Animation.WoWoTranslationAnimation;
import com.nightonke.wowoviewpager.Enum.Ease;
import com.nightonke.wowoviewpager.WoWoPathView;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;

public class GuidePageActivity1 extends WoWoActivity {

    private int r;
    private boolean animationAdded = false;

    @Override
    protected int contentViewRes() {
        return ResourceTable.Layout_activity_guide_page1;
    }

    @Override
    protected int fragmentNumber() {
        return 4;
    }

    @Override
    protected Integer[] fragmentColorsRes() {
        return new Integer[]{
                ResourceTable.Color_black_background,
                ResourceTable.Color_black_background,
                ResourceTable.Color_black_background,
                ResourceTable.Color_black_background,
        };
    }


    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        r = (int) Math.sqrt(screenW * screenW + screenH * screenH) + 10;

        wowo.addTemporarilyInvisibleViews(1, findComponentById(ResourceTable.Id_little_cloud), findComponentById(ResourceTable.Id_big_cloud));
        wowo.addTemporarilyInvisibleViews(2, findComponentById(ResourceTable.Id_sun), findComponentById(ResourceTable.Id_nightonke_cloud));
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        addAnimations();
    }

    private void addAnimations() {
        if (animationAdded) return;
        animationAdded = true;

        addCircleAnimation();
        addNightonkeAnimation();
        addPresentsAnimation();
        addBlueStickAnimation();
        addOrangeStickAnimation();
        addWoWoTextAnimation();
        addViewPagerTextAnimation();
        addMusicStandAnimation();
        addMusicNotesAnimation();
        addBigCloudAnimation();
        addLittleCloudAnimation();
        addPathAnimation();
        addSloganAnimation();
        addSunAnimation();
        addNightonkeCloudAnimation();
        wowo.ready();
    }

    private void addCircleAnimation() {
        Component circle = findComponentById(ResourceTable.Id_circle);
        int width = circle.getWidth();
        int height = circle.getHeight();
        wowo.addAnimation(circle)
                .add(WoWoShapeColorAnimation.builder().page(0)
                        .from(getContext().getColor(ResourceTable.Color_gray)).to(getContext().getColor(ResourceTable.Color_light_blue)).build())
                .add(WoWoScaleAnimation.builder().page(0)
                        .fromXY(1).toX(r * 2 / width).toY(r * 2 / height)
                        .ease(Ease.InBack).sameEaseBack(false).build());
    }

    private void addNightonkeAnimation() {
        Component nightonke = findComponentById(ResourceTable.Id_nightonke);
        wowo.addAnimation(nightonke)
                .add(WoWoTranslationAnimation.builder().page(0)
                        .fromX(nightonke.getTranslationX()).toX(-screenW)
                        .keepY(nightonke.getTranslationY())
                        .ease(Ease.InBack).sameEaseBack(false).build());
    }

    private void addPresentsAnimation() {
        Component presents = findComponentById(ResourceTable.Id_presents);
        wowo.addAnimation(presents)
                .add(WoWoTranslationAnimation.builder().page(0)
                        .fromX(presents.getTranslationX()).toX(screenW)
                        .keepY(presents.getTranslationY())
                        .ease(Ease.InBack).sameEaseBack(false).build());
    }

    private void addBlueStickAnimation() {
        Component blueStick = findComponentById(ResourceTable.Id_blue_stick);
        wowo.addAnimation(blueStick)
                .add(WoWoTranslationAnimation.builder().page(0)
                        .keepX(blueStick.getTranslationX())
                        .fromY(blueStick.getTranslationY()).toY(screenH)
                        .ease(Ease.InCubic).sameEaseBack(false).build());
    }

    private void addOrangeStickAnimation() {
        Component orangeStick = findComponentById(ResourceTable.Id_orange_stick);
        wowo.addAnimation(orangeStick)
                .add(WoWoTranslationAnimation.builder().page(0)
                        .fromX(orangeStick.getTranslationX()).toX(-screenW)
                        .keepY(orangeStick.getTranslationY())
                        .ease(Ease.InCubic).sameEaseBack(false).build());
    }

    private void addWoWoTextAnimation() {
        Component wowoText = findComponentById(ResourceTable.Id_wowo);
        wowo.addAnimation(wowoText)
                .add(WoWoRotationAnimation.builder().page(0)
                        .keepX(0).keepY(0)
                        .fromZ(-15).toZ(-150)
                        .ease(Ease.InBack).sameEaseBack(false).build())
                .add(WoWoTranslationAnimation.builder().page(0)
                        .fromX(wowoText.getTranslationX()).toX(-screenW)
                        .keepY(wowoText.getTranslationY())
                        .ease(Ease.InBack).sameEaseBack(false).build());
    }

    private void addViewPagerTextAnimation() {
        Component viewPagerText = findComponentById(ResourceTable.Id_viewpager);
        wowo.addAnimation(viewPagerText)
                .add(WoWoRotationAnimation.builder().page(0)
                        .keepX(0).keepY(0)
                        .fromZ(15).toZ(150)
                        .ease(Ease.InBack).sameEaseBack(false).build())
                .add(WoWoTranslationAnimation.builder().page(0)
                        .fromX(viewPagerText.getTranslationX()).toX(-screenW)
                        .keepY(viewPagerText.getTranslationY())
                        .ease(Ease.InBack).sameEaseBack(false).build());
    }

    private void addMusicStandAnimation() {
        Component musicStand = findComponentById(ResourceTable.Id_music_stand);
        wowo.addAnimation(musicStand)
                .add(WoWoTranslationAnimation.builder().page(0).start(0.3)
                        .fromX(screenW).toX(0).keepY(0).build())
                .add(WoWoTranslationAnimation.builder().page(1).end(0.5)
                        .keepX(0).fromY(0).toY(screenH).build());
    }

    private void addMusicNotesAnimation() {
        Component musicNotes = findComponentById(ResourceTable.Id_music_notes);
        wowo.addAnimation(musicNotes)
                .add(WoWoTranslationAnimation.builder().page(0).start(0.5)
                        .fromX(screenW).toX(0).keepY(0).build())
                .add(WoWoTranslationAnimation.builder().page(1).end(0.5)
                        .keepX(0).fromY(0).toY(screenH).build());
    }

    private void addBigCloudAnimation() {
        Component bigCloud = findComponentById(ResourceTable.Id_big_cloud);
        wowo.addAnimation(bigCloud)
                .add(WoWoTranslationAnimation.builder().page(1)
                        .keepX(0)
                        .fromY(-screenH / 2).toY(0).ease(Ease.OutBack).build())
                .add(WoWoTranslationAnimation.builder().page(2)
                        .fromX(0).toX(-screenW)
                        .keepY(0).ease(Ease.InBack).sameEaseBack(false).build());
    }

    private void addLittleCloudAnimation() {
        Component littleCloud = findComponentById(ResourceTable.Id_little_cloud);
        wowo.addAnimation(littleCloud)
                .add(WoWoTranslationAnimation.builder().page(1)
                        .keepX(0)
                        .fromY(-screenH / 2).toY(0).ease(Ease.OutBack).build())
                .add(WoWoTranslationAnimation.builder().page(2)
                        .fromX(0).toX(-screenW)
                        .keepY(0).ease(Ease.InBack).sameEaseBack(false).build());

    }

    private void addPathAnimation() {
        WoWoPathView pathView = (WoWoPathView) findComponentById(ResourceTable.Id_path_view);

        // For different screen size, try to adjust the scale values to see the airplane.
        float xScale = screenW / 720f;
        float yScale = screenH / 1280f;

        pathView.newPath()
                .pathMoveTo(screenW / 2, screenH + yScale * 100)
                .pathCubicTo(xScale * 313, screenH - yScale * 531,
                        xScale * (-234), screenH - yScale * 644,
                        xScale * 141, screenH - yScale * 772)
                .pathCubicTo(xScale * 266, screenH - yScale * 817,
                        xScale * 444, screenH - yScale * 825,
                        xScale * 596, screenH - yScale * 788)
                .pathCubicTo(xScale * 825, screenH - yScale * 727,
                        xScale * 755, screenH - yScale * 592,
                        xScale * (-100), screenH - yScale * 609);
        wowo.addAnimation(pathView)
                .add(WoWoPathAnimation.builder().page(1).from(0).to(1).path(pathView).build())
                .add(WoWoAlphaAnimation.builder().page(2).from(1).to(0).build());
    }

    private void addSloganAnimation() {
        Component slogan = findComponentById(ResourceTable.Id_slogan);
        slogan.setAlpha(0);
        String fromResource = getContext().getString(ResourceTable.String_optimized);
        String toResource = getContext().getString(ResourceTable.String_free);

        wowo.addAnimation(slogan)
                .add(WoWoAlphaAnimation.builder().page(1).from(0).to(1).build())
                .add(WoWoTextViewTextAnimation.builder().page(2)
                        .from(fromResource.toString())
                        .to(toResource.toString()).build());
    }

    private void addSunAnimation() {
        Component sun = findComponentById(ResourceTable.Id_sun);
        wowo.addAnimation(sun)
                .add(WoWoTranslationAnimation.builder().page(2)
                        .fromX(screenW).toX(0)
                        .fromY(-screenW).toY(0).build())
                .add(WoWoRotationAnimation.builder().page(2)
                        .keepX(0)
                        .keepY(0)
                        .fromZ(0).toZ(360 * 4)
                        .ease(Ease.OutBack).sameEaseBack(false).build());
    }

    private void addNightonkeCloudAnimation() {
        Component cloud = findComponentById(ResourceTable.Id_nightonke_cloud);
        wowo.addAnimation(cloud)
                .add(WoWoTranslationAnimation.builder().page(2)
                        .fromX(screenW).toX(0)
                        .keepY(0).ease(Ease.OutBack).build());
    }
}
