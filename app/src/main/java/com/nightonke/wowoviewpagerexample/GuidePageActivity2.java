package com.nightonke.wowoviewpagerexample;

import com.nightonke.wowoviewpager.Animation.WoWoAlphaAnimation;
import com.nightonke.wowoviewpager.Animation.WoWoElevationAnimation;
import com.nightonke.wowoviewpager.Animation.WoWoPathAnimation;
import com.nightonke.wowoviewpager.Animation.WoWoRotationAnimation;
import com.nightonke.wowoviewpager.Animation.WoWoScaleAnimation;
import com.nightonke.wowoviewpager.Animation.WoWoShapeColorAnimation;
import com.nightonke.wowoviewpager.Animation.WoWoTextViewColorAnimation;
import com.nightonke.wowoviewpager.Animation.WoWoTextViewTextAnimation;
import com.nightonke.wowoviewpager.Animation.WoWoTranslationAnimation;
import com.nightonke.wowoviewpager.Enum.Ease;
import com.nightonke.wowoviewpager.WoWoPathView;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.PageSlider;
import ohos.agp.components.TextField;
import ohos.agp.utils.Color;

import java.lang.reflect.Method;

public class GuidePageActivity2 extends WoWoActivity {

    private int r;
    private boolean animationAdded = false;
    private Image targetPlanet;
    private Component loginLayout;
    private Component cloud_yellow;
    private Component earth;
    private TextField username;
    private TextField password;

    @Override
    protected int contentViewRes() {
        return ResourceTable.Layout_activity_guide_page2;
    }

    @Override
    protected int fragmentNumber() {
        return 4;
    }

    @Override
    protected Integer[] fragmentColorsRes() {
        return new Integer[]{
                ResourceTable.Color_white,
                ResourceTable.Color_white,
                ResourceTable.Color_white,
                ResourceTable.Color_white

        };
    }


    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);

        r = (int) Math.sqrt(screenW * screenW + screenH * screenH) + 10;

        Image earth = (Image) findComponentById(ResourceTable.Id_earth);
        targetPlanet = (Image) findComponentById(ResourceTable.Id_planet_target);
        loginLayout = findComponentById(ResourceTable.Id_login_layout);
        cloud_yellow = findComponentById(ResourceTable.Id_cloud_yellow);
        username = (TextField) findComponentById(ResourceTable.Id_username);
        password = (TextField) findComponentById(ResourceTable.Id_password);
        earth.setHeight(screenW);
        earth.setContentPositionY(screenH / 2);
        targetPlanet.setContentPositionY(-screenH / 2 - screenW / 2);
        targetPlanet.setScaleX(0.25f);
        targetPlanet.setScaleY(0.25f);

//        wowo.addTemporarilyInvisibleViews(0, earth, findComponentById(ResourceTable.Id_cloud_blue), findComponentById(ResourceTable.Id_cloud_red));
//        wowo.addTemporarilyInvisibleViews(0, targetPlanet);
        wowo.addTemporarilyInvisibleViews(2, loginLayout, findComponentById(ResourceTable.Id_button));
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        addAnimations();
    }

    private void addAnimations() {
        if (animationAdded) return;
        animationAdded = true;

        addEarthAnimation();
        addCloudAnimation();
        addTextAnimation();
        addRocketAnimation();
        addCircleAnimation();
        addMeteorAnimation();
        addPlanetAnimation();
        addPlanetTargetAnimation();
        addLoginLayoutAnimation();
        addButtonAnimation();
        addEditTextAnimation();

        wowo.ready();

        // Do this the prevent the edit-text and button views on login layout
        // to intercept the drag event.
        wowo.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int position, float positionOffset, int positionOffsetPixels) {
                loginLayout.setEnabled(position == 3);
                loginLayout.setVisibility(position + positionOffset <= 2 ? Component.INVISIBLE : Component.VISIBLE);


            }

            @Override
            public void onPageSlideStateChanged(int position) {
                getContext().getUITaskDispatcher().asyncDispatch(new Runnable() {
                    @Override
                    public void run() {
                        if (wowo.getCurrentPage() >= 2) {
                            cloud_yellow.setVisibility(Component.INVISIBLE);
                        } else {
                            cloud_yellow.setVisibility(Component.VISIBLE);
                        }
                        if (wowo.getCurrentPage() >= 3) {
                            password.setEnabled(true);
                            username.setEnabled(true);
                        } else {
                            password.setEnabled(false);
                            password.clearFocus();
                            username.setEnabled(false);
                            username.clearFocus();
                            earth.setVisibility(Component.VISIBLE);
                        }
                    }
                });
            }

            @Override
            public void onPageChosen(int i) {

            }
        });
    }

    private void addEarthAnimation() {
        earth = findComponentById(ResourceTable.Id_earth);
        wowo.addAnimation(earth)
                .add(WoWoRotationAnimation.builder().page(0).keepX(0).keepY(0).fromZ(0).toZ(180).ease(Ease.OutBack).build())
                .add(WoWoRotationAnimation.builder().page(1).keepX(0).keepY(0).fromZ(180).toZ(720).ease(Ease.OutBack).build())
                .add(WoWoRotationAnimation.builder().page(2).keepX(0).keepY(0).fromZ(720).toZ(1260).ease(Ease.OutBack).build())
                .add(WoWoScaleAnimation.builder().page(1).fromXY(1).toXY(0.5).ease(Ease.OutBack).build())
                .add(WoWoScaleAnimation.builder().page(2).fromXY(0.5).toXY(0.25).ease(Ease.OutBack).build())
                .add(WoWoScaleAnimation.builder().page(3).fromXY(0.25).toXY(0.1).ease(Ease.OutBack).build());
    }

    private void addCloudAnimation() {
        wowo.addAnimation(findComponentById(ResourceTable.Id_cloud_blue))
                .add(WoWoTranslationAnimation.builder().page(0).fromX(screenW).toX(0).keepY(0).ease(Ease.OutBack).sameEaseBack(false).build())
                .add(WoWoTranslationAnimation.builder().page(1).fromX(0).toX(screenW - 280).keepY(0).ease(Ease.InBack).sameEaseBack(false).build());

        wowo.addAnimation(findComponentById(ResourceTable.Id_cloud_red))
                .add(WoWoTranslationAnimation.builder().page(0).fromX(-screenW).toX(0).keepY(0).ease(Ease.OutBack).sameEaseBack(false).build())
                .add(WoWoTranslationAnimation.builder().page(1).fromX(0).toX(-screenW).keepY(0).ease(Ease.InBack).sameEaseBack(false).build());

        wowo.addAnimation(cloud_yellow)
                .add(WoWoTranslationAnimation.builder().page(0).keepX(0).fromY(cloud_yellow.getTranslationY()).toY(cloud_yellow.getTranslationY() + 180).ease(Ease.InBack).sameEaseBack(false).build())
                .add(WoWoTranslationAnimation.builder().page(1).fromX(0).toX(-screenW - 80).keepY(cloud_yellow.getTranslationY() + 180).ease(Ease.InBack).sameEaseBack(false).build())
                .add(WoWoTranslationAnimation.builder().page(2).fromX(-screenW - 80).toX(0).keepY(0).ease(Ease.OutBack).sameEaseBack(false).build());
    }

    private void addTextAnimation() {
        Component text = findComponentById(ResourceTable.Id_text);
//         text.setZ(50);
        String[] texts = new String[]{
                "HOME?",
                "OR SKY?",
                "OR UNIVERSE?",
                "Let's Discover More!",
        };
        wowo.addAnimation(text)
                .add(WoWoTextViewTextAnimation.builder().page(0).from(texts[0]).to(texts[1]).build())
                .add(WoWoTextViewTextAnimation.builder().page(1).from(texts[1]).to(texts[2]).build())
                .add(WoWoTextViewTextAnimation.builder().page(2).from(texts[2]).to(texts[3]).build())
                .add(WoWoTextViewColorAnimation.builder().page(1).from("#05502f").to(Color.WHITE.getValue()).build());
    }

    private void addRocketAnimation() {
        WoWoPathView pathView = (WoWoPathView) findComponentById(ResourceTable.Id_path_view);
//        pathView.setZ(50);

        // For different screen size, try to adjust the scale values to see the airplane.
        float xScale = 1;
        float yScale = 1;

        pathView.newPath()
                .pathMoveTo(xScale * (-100), screenH - 100)
                .pathCubicTo(screenW / 2, screenH - 100,
                        screenW / 2, screenH - 100,
                        screenW / 2, yScale * (-100));
        wowo.addAnimation(pathView)
                .add(WoWoPathAnimation.builder().page(0).from(0).to(0.50).path(pathView).build())
                .add(WoWoPathAnimation.builder().page(1).from(0.50).to(0.75).path(pathView).build())
                .add(WoWoPathAnimation.builder().page(2).from(0.75).to(1).path(pathView).build())
                .add(WoWoAlphaAnimation.builder().page(2).from(1).to(0).build());
    }

    private void addCircleAnimation() {
        Component circle = findComponentById(ResourceTable.Id_circle);
        wowo.addAnimation(circle)
                .add(WoWoScaleAnimation.builder().page(1).fromXY(1).toXY(r * 2 / circle.getWidth()).build())
                .add(WoWoShapeColorAnimation.builder().page(1).from("#f9dc0a").to("#05502f").build());
    }

    private void addMeteorAnimation() {
        Component meteor = findComponentById(ResourceTable.Id_meteor);
        float fullOffset = screenW + meteor.getWidth();
        float offset = fullOffset / 2;
        wowo.addAnimation(meteor)
                .add(WoWoTranslationAnimation.builder().page(1)
                        .fromX(0).fromY(0)
                        .toX(offset).toY(offset).ease(Ease.OutBack).sameEaseBack(false).build())
                .add(WoWoTranslationAnimation.builder().page(2)
                        .fromX(offset).fromY(offset)
                        .toX(fullOffset).toY(fullOffset).ease(Ease.InBack).sameEaseBack(false).build());
    }

    private void addPlanetAnimation() {
        Component planet0 = findComponentById(ResourceTable.Id_planet_0);
        wowo.addAnimation(planet0)
                .add(WoWoTranslationAnimation.builder().page(1)
                        .keepX(0)
                        .fromY(0).toY(planet0.getHeight() + 200)
                        .ease(Ease.OutBack).sameEaseBack(false).build())
                .add(WoWoTranslationAnimation.builder().page(2)
                        .fromX(0).toX(screenW)
                        .keepY(planet0.getHeight() + 200)
                        .ease(Ease.InBack).sameEaseBack(false).build());

        Component planet1 = findComponentById(ResourceTable.Id_planet_1);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) planet1.setZ(50);
        planet1.setMarginRight(-planet1.getWidth());
        wowo.addAnimation(planet1)
                .add(WoWoTranslationAnimation.builder().page(1)
                        .fromX(0).toX(-planet1.getEstimatedWidth())
                        .keepY(0)
                        .ease(Ease.OutBack).sameEaseBack(false).build())
                .add(WoWoTranslationAnimation.builder().page(2)
                        .fromX(-planet1.getWidth()).toX(-screenW - planet1.getWidth())
                        .keepY(0)
                        .ease(Ease.InBack).sameEaseBack(false).build());
    }

    private void addPlanetTargetAnimation() {
        wowo.addAnimation(targetPlanet)
                .add(WoWoRotationAnimation.builder().page(1).keepX(0).keepY(0).fromZ(0).toZ(180).ease(Ease.OutBack).build())
                .add(WoWoRotationAnimation.builder().page(2).keepX(0).keepY(0).fromZ(180).toZ(360).ease(Ease.OutBack).build())
                .add(WoWoTranslationAnimation.builder().page(0).keepX(0)
                        .fromY(-screenH / 2 - screenW / 2)
                        .toY(-targetPlanet.getEstimatedHeight() / 2 - 30).ease(Ease.OutBack).sameEaseBack(false).build())
                .add(WoWoTranslationAnimation.builder().page(1).keepX(0)
                        .fromY(-targetPlanet.getEstimatedHeight() / 2 - 30)
                        .toY(-targetPlanet.getEstimatedHeight() / 2).ease(Ease.InBack).sameEaseBack(false).build())
                .add(WoWoScaleAnimation.builder().page(0).fromXY(0).toXY(0.25).ease(Ease.OutBack).build())
                .add(WoWoScaleAnimation.builder().page(1).fromXY(0.25).toXY(0.5).ease(Ease.OutBack).build())
                .add(WoWoScaleAnimation.builder().page(2).fromXY(0.5).toXY(1.2).ease(Ease.OutBack).build());
    }

    private void addLoginLayoutAnimation() {
        Component layout = findComponentById(ResourceTable.Id_login_layout);
        wowo.addAnimation(layout)
                .add(WoWoAlphaAnimation.builder().page(1).start(1).end(1).from(0).to(1).build())
                .add(WoWoShapeColorAnimation.builder().page(2).from("#05502f").to("#0aa05f").build())
                .add(WoWoElevationAnimation.builder().page(2).from(0).to(40).build());
    }

    private void addButtonAnimation() {
        Component button = findComponentById(ResourceTable.Id_button);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) button.setZ(50);
        wowo.addAnimation(button)
                .add(WoWoAlphaAnimation.builder().page(2).from(0).to(1).build());
    }

    private void addEditTextAnimation() {
        wowo.addAnimation(username)
                .add(WoWoAlphaAnimation.builder().page(2).from(0).to(1).build());
        wowo.addAnimation(findComponentById(ResourceTable.Id_password))
                .add(WoWoAlphaAnimation.builder().page(2).from(0).to(1).build());
    }

    public static boolean showSoftInput() {
        try {
            Class inputClass = Class.forName("ohos.miscservices.inputmethod.InputMethodController");
            Method method = inputClass.getMethod("getInstance");
            Object object = method.invoke(new Object[]{});
            Method startInput = inputClass.getMethod("startInput", int.class, boolean.class);
            return (boolean) startInput.invoke(object, 1, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean hideSoftInput() {
        try {
            Class inputClass = Class.forName("ohos.miscservices.inputmethod.InputMethodController");
            Method method = inputClass.getMethod("getInstance");
            Object object = method.invoke(new Object[]{});
            Method stopInput = inputClass.getMethod("stopInput", int.class);
            return (boolean) stopInput.invoke(object, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
