package com.nightonke.wowoviewpagerexample;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Checkbox;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;

public class SetEaseTypeActivity extends Ability implements ListContainer.ItemClickedListener {

    private Checkbox checkBox;
    private String[] strList = new String[]{"Linear",
            "Ease In Sine",
            "Ease Out Sine",
            "Ease In Out Sine",
            "Ease In Quad",
            "Ease Out Quad",
            "Ease In Out Quad",
            "Ease In Cubic",
            "Ease Out Cubic",
            "Ease In Out Cubic",
            "Ease In Quart",
            "Ease Out Quart",
            "Ease In Out Quart",
            "Ease In Quint",
            "Ease Out Quint",
            "Ease In Out Quint",
            "Ease In Expo",
            "Ease Out Expo",
            "Ease In Out Expo",
            "Ease In Circ",
            "Ease Out Circ",
            "Ease In Out Circ",
            "Ease In Back",
            "Ease Out Back",
            "Ease In Out Back",
            "Ease In Elastic",
            "Ease Out Elastic",
            "Ease In Out Elastic",
            "Ease In Bounce",
            "Ease Out Bounce",
            "Ease In Out Bounce"};
    private Intent cuurrntIntent;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_set_ease_type);
        cuurrntIntent = intent;

        ListContainer listView = (ListContainer) findComponentById(ResourceTable.Id_listview);
        checkBox = (Checkbox) findComponentById(ResourceTable.Id_checkbox);
        checkBox.setChecked(true);
        ArrayList<String> mlist = new ArrayList<>();
        for (int i = 0; i < strList.length; i++) {
            mlist.add(strList[i]);
        }
        listView.setItemProvider(new SetEaseTypeAdapter(mlist, this));
        listView.setItemClickedListener(this);

    }

    @Override
    public void onItemClicked(ListContainer listContainer, Component component, int position, long l) {
        Intent intent = new Intent();
        Operation operation = null;
        switch (cuurrntIntent.getStringParam("AnimationType")) {
            case "WoWoPositionAnimation":
                operation = new Intent.OperationBuilder()
                        .withBundleName("com.nightonke.wowoviewpagerexample")
                        .withAbilityName("com.nightonke.wowoviewpagerexample.WoWoPositionAnimationActivity")
                        .build();
                break;
            case "WoWoTranslationAnimation":
                operation = new Intent.OperationBuilder()
                        .withBundleName("com.nightonke.wowoviewpagerexample")
                        .withAbilityName("com.nightonke.wowoviewpagerexample.WoWoTranslationAnimationActivity")
                        .build();
                break;
            case "WoWoScaleAnimation":
                operation = new Intent.OperationBuilder()
                        .withBundleName("com.nightonke.wowoviewpagerexample")
                        .withAbilityName("com.nightonke.wowoviewpagerexample.WoWoScaleAnimationActivity")
                        .build();
                break;
            case "WoWoAlphaAnimation":
                operation = new Intent.OperationBuilder()
                        .withBundleName("com.nightonke.wowoviewpagerexample")
                        .withAbilityName("com.nightonke.wowoviewpagerexample.WoWoAlphaAnimationActivity")
                        .build();
                break;
            case "WoWoRotationAnimation":
                operation = new Intent.OperationBuilder()
                        .withBundleName("com.nightonke.wowoviewpagerexample")
                        .withAbilityName("com.nightonke.wowoviewpagerexample.WoWoRotationAnimationActivity")
                        .build();
                break;
            case "WoWoElevationAnimation":
                operation = new Intent.OperationBuilder()
                        .withBundleName("com.nightonke.wowoviewpagerexample")
                        .withAbilityName("com.nightonke.wowoviewpagerexample.WoWoElevationAnimationActivity")
                        .build();
                break;
            case "WoWoTextViewTextSizeAnimation":
                operation = new Intent.OperationBuilder()
                        .withBundleName("com.nightonke.wowoviewpagerexample")
                        .withAbilityName("com.nightonke.wowoviewpagerexample.WoWoTextViewTextSizeAnimationActivity")
                        .build();
                break;
            case "WoWoTextViewColorAnimation":
                operation = new Intent.OperationBuilder()
                        .withBundleName("com.nightonke.wowoviewpagerexample")
                        .withAbilityName("com.nightonke.wowoviewpagerexample.WoWoTextViewColorAnimationActivity")
                        .build();
                break;
            case "WoWoTextViewTextAnimation":
                operation = new Intent.OperationBuilder()
                        .withBundleName("com.nightonke.wowoviewpagerexample")
                        .withAbilityName("com.nightonke.wowoviewpagerexample.WoWoTextViewTextAnimationActivity")
                        .build();
                break;
            case "WoWoBackgroundColorAnimation":
                operation = new Intent.OperationBuilder()
                        .withBundleName("com.nightonke.wowoviewpagerexample")
                        .withAbilityName("com.nightonke.wowoviewpagerexample.WoWoBackgroundColorAnimationActivity")
                        .build();
                break;
            case "WoWoShapeColorAnimation":
                operation = new Intent.OperationBuilder()
                        .withBundleName("com.nightonke.wowoviewpagerexample")
                        .withAbilityName("com.nightonke.wowoviewpagerexample.WoWoShapeColorAnimationActivity")
                        .build();
                break;
            case "WoWoLayerListColorAnimation":
                operation = new Intent.OperationBuilder()
                        .withBundleName("com.nightonke.wowoviewpagerexample")
                        .withAbilityName("com.nightonke.wowoviewpagerexample.WoWoLayerListColorAnimationActivity")
                        .build();
                break;
            case "WoWoStateListColorAnimation":
                operation = new Intent.OperationBuilder()
                        .withBundleName("com.nightonke.wowoviewpagerexample")
                        .withAbilityName("com.nightonke.wowoviewpagerexample.WoWoStateListColorAnimationActivity")
                        .build();
                break;
            case "WoWoPathAnimation":
                operation = new Intent.OperationBuilder()
                        .withBundleName("com.nightonke.wowoviewpagerexample")
                        .withAbilityName("com.nightonke.wowoviewpagerexample.WoWoPathAnimationActivity")
                        .build();
                break;
            case "CustomAnimation":
                operation = new Intent.OperationBuilder()
                        .withBundleName("com.nightonke.wowoviewpagerexample")
                        .withAbilityName("com.nightonke.wowoviewpagerexample.CustomAnimationActivity")
                        .build();
                break;
            default:
                return;
        }
        intent.setOperation(operation);
        switch (listContainer.getId()) {
            case ResourceTable.Id_listview:
                intent.setParam("easeType", position);
                intent.setParam("useSameEaseTypeBack", checkBox.isChecked());
                startAbility(intent);
                break;
        }
    }
}
