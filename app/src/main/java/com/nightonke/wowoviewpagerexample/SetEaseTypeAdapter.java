package com.nightonke.wowoviewpagerexample;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

import java.util.List;

public class SetEaseTypeAdapter extends BaseItemProvider {
    List<String> mList;
    private Context mContext;

    public SetEaseTypeAdapter(List<String> list, Context context) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
        final Component cpt;
        if (convertComponent == null) {
            cpt = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_item_list, null, false);
        } else {
            cpt = convertComponent;
        }
        String s = mList.get(position);
        Text componentById = (Text) cpt.findComponentById(ResourceTable.Id_text);
        componentById.setText(s);
        return cpt;
    }

}
