package com.nightonke.wowoviewpagerexample;

import ohos.aafwk.content.Intent;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.Slider;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.element.ShapeElement;

public class StaticActivity extends GuidePageActivity1 {


    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        wowo.setDraggable(false);
        wowo.setScrollDuration(1000);
        DependentLayout base = (DependentLayout) findComponentById(ResourceTable.Id_base);

        Slider seekBar = new Slider(this);
        DependentLayout.LayoutConfig layoutParams = new DependentLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                ComponentContainer.LayoutConfig.MATCH_CONTENT);
        layoutParams.setMargins(0, 0, 0, 20);
        layoutParams.addRule(DependentLayout.LayoutConfig.ALIGN_PARENT_BOTTOM);
        seekBar.setLayoutConfig(layoutParams);

        seekBar.setMaxValue(9);
        seekBar.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean fromUser) {
                if (fromUser) wowo.setScrollDuration(progress * 500);
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });

        seekBar.setProgressValue(2);
        base.addComponent(seekBar);
        Button nextButton = new Button(this);
        DependentLayout.LayoutConfig layoutParams2 = new DependentLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT,
                ComponentContainer.LayoutConfig.MATCH_CONTENT);
        layoutParams2.setMargins(0, 0, 20, 80);
        layoutParams2.addRule(DependentLayout.LayoutConfig.ALIGN_PARENT_BOTTOM);
        layoutParams2.addRule(DependentLayout.LayoutConfig.ALIGN_PARENT_RIGHT);
        nextButton.setLayoutConfig(layoutParams2);
        nextButton.setText("Next");
        nextButton.setPadding(50, 20, 50, 20);
        nextButton.setMarginBottom(100);
        nextButton.setTextSize(30);
        ShapeElement shapeElement = new ShapeElement(getContext(), ResourceTable.Graphic_background_white_rec);
        nextButton.setBackground(shapeElement);
        nextButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                wowo.next();
            }
        });
        base.addComponent(nextButton);
        Button previousButton = new Button(this);
        DependentLayout.LayoutConfig layoutParams3 = new DependentLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT,
                ComponentContainer.LayoutConfig.MATCH_CONTENT);
        layoutParams3.setMargins(20, 0, 0, 80);
        layoutParams3.addRule(DependentLayout.LayoutConfig.ALIGN_PARENT_BOTTOM);
        layoutParams3.addRule(DependentLayout.LayoutConfig.ALIGN_PARENT_LEFT);
        previousButton.setLayoutConfig(layoutParams3);
        previousButton.setText("Previous");
        previousButton.setPadding(40, 20, 40, 20);
        previousButton.setMarginBottom(100);
        previousButton.setTextSize(30);
        ShapeElement shapeElement2 = new ShapeElement(getContext(), ResourceTable.Graphic_background_white_rec);
        previousButton.setBackground(shapeElement2);
        previousButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                wowo.previous();
            }
        });
        base.addComponent(previousButton);

    }
}
