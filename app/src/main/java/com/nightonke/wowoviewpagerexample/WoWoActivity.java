package com.nightonke.wowoviewpagerexample;

import com.nightonke.wowoviewpager.Enum.Ease;
import com.nightonke.wowoviewpager.WoWoViewPagerAdapter;
import com.nightonke.wowoviewpager.WoWoViewPager;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.PageSlider;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;

public abstract class WoWoActivity extends Ability {

    protected WoWoViewPager wowo;
    public  int ease = 30;
    protected boolean useSameEaseTypeBack = true;

    protected Text pageNumber;

    protected abstract int contentViewRes();

    protected int fragmentNumber() {
        return 5;
    }

    protected Integer[] fragmentColorsRes() {
        return new Integer[]{
                ResourceTable.Color_blue_1,
                ResourceTable.Color_blue_2,
                ResourceTable.Color_blue_3,
                ResourceTable.Color_blue_4,
                ResourceTable.Color_blue_5,
        };
    }

    protected int screenW;
    protected int screenH;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
//        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setUIContent(contentViewRes());

        init();

        wowo = (WoWoViewPager)findComponentById(ResourceTable.Id_wowo_viewpager);
        wowo.setProvider(WoWoViewPagerAdapter.builder()
                .count(fragmentNumber())                       // Fragment Count
                .colorsRes(fragmentColorsRes())                // Colors of fragments
                .build(this));
        wowo.ready();
        setPageTV(wowo);
        pageNumber = (Text) findComponentById(ResourceTable.Id_page);

        screenW = WoWoUtil.getScreenWidth(this);
        screenH = WoWoUtil.getScreenHeight(this);
    }


    protected int color(int colorRes) {

//        return ContextCompat.getColor(this, colorRes);
        return new Color(colorRes).getValue();
    }

    private void setPageTV(WoWoViewPager wowo) {
        wowo.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int i, float v, int i1) {

            }

            @Override
            public void onPageSlideStateChanged(int i) {

            }

            @Override
            public void onPageChosen(int position) {
                if (pageNumber != null) pageNumber.setText(String.valueOf(position));
            }
        });
    }

    public void init() {
        useSameEaseTypeBack = getIntent().getBooleanParam("useSameEaseTypeBack", true);
        int easeEnumNumber = getIntent().getIntParam("easeType", -1);
        switch (easeEnumNumber) {
            case 0: ease = Ease.Linear; break;
            case 1: ease = Ease.InSine; break;
            case 2: ease = Ease.OutSine; break;
            case 3: ease = Ease.InOutSine; break;
            case 4: ease = Ease.InQuad; break;
            case 5: ease = Ease.OutQuad; break;
            case 6: ease = Ease.InOutQuad; break;
            case 7: ease = Ease.InCubic; break;
            case 8: ease = Ease.OutCubic; break;
            case 9: ease = Ease.InOutCubic; break;
            case 10: ease = Ease.InQuart; break;
            case 11: ease = Ease.OutQuart; break;
            case 12: ease = Ease.InOutQuart; break;
            case 13: ease = Ease.InQuint; break;
            case 14: ease = Ease.OutQuint; break;
            case 15: ease = Ease.InOutQuint; break;
            case 16: ease = Ease.InExpo; break;
            case 17: ease = Ease.OutExpo; break;
            case 18: ease = Ease.InOutExpo; break;
            case 19: ease = Ease.InCirc; break;
            case 20: ease = Ease.OutCirc; break;
            case 21: ease = Ease.InOutCirc; break;
            case 22: ease = Ease.InBack; break;
            case 23: ease = Ease.OutBack; break;
            case 24: ease = Ease.InOutBack; break;
            case 25: ease = Ease.InElastic; break;
            case 26: ease = Ease.OutElastic; break;
            case 27: ease = Ease.InOutElastic; break;
            case 28: ease = Ease.InBounce; break;
            case 29: ease = Ease.OutBounce; break;
            case 30: ease = Ease.InOutBounce; break;
        }
    }

    protected int dp2px(float dp) {
        return WoWoUtil.dp2px((int) dp, this);
    }

    protected int dp2px(double dp) {
        return WoWoUtil.dp2px((int) dp, this);
    }
}
