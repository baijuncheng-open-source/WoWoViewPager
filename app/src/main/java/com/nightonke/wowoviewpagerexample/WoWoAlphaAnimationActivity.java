package com.nightonke.wowoviewpagerexample;


import com.nightonke.wowoviewpager.Animation.ViewAnimation;
import com.nightonke.wowoviewpager.Animation.WoWoAlphaAnimation;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;

public class WoWoAlphaAnimationActivity extends WoWoActivity {

    @Override
    protected int contentViewRes() {
        return ResourceTable.Layout_activity_wowo_alpha_animation;

    }
    @Override
    protected Integer[] fragmentColorsRes() {
        return new Integer[]{  ResourceTable.Color_white,
                ResourceTable.Color_white,
                ResourceTable.Color_white,
                ResourceTable.Color_white,
                ResourceTable.Color_white,};
    }


    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        Component view = findComponentById(ResourceTable.Id_test);
        ViewAnimation viewAnimation = new ViewAnimation(view);
        viewAnimation.add(WoWoAlphaAnimation.builder().page(0).from(1).to(0.5).build());
        viewAnimation.add(WoWoAlphaAnimation.builder().page(1).from(0.5).to(1).build());
        viewAnimation.add(WoWoAlphaAnimation.builder().page(2).start(0).end(0.5).from(1).to(0).build());
        viewAnimation.add(WoWoAlphaAnimation.builder().page(2).start(0.5).end(1).from(0).to(1).build());
        viewAnimation.add(WoWoAlphaAnimation.builder().page(3).start(0).end(0.5).from(1).to(0.3).build());
        viewAnimation.add(WoWoAlphaAnimation.builder().page(3).start(0.5).end(1).from(0.3).to(1).build());
        wowo.addAnimation(viewAnimation);

        wowo.setEase(ease);
        wowo.setUseSameEaseBack(useSameEaseTypeBack);
        wowo.ready();

    }
}
