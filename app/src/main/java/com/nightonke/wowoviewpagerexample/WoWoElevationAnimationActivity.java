package com.nightonke.wowoviewpagerexample;


import com.nightonke.wowoviewpager.Animation.WoWoElevationAnimation;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;

public class WoWoElevationAnimationActivity extends WoWoActivity {


    @Override
    protected int contentViewRes() {
        return ResourceTable.Layout_activity_wowo_elevation_animation;

    }
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        Text text = (Text) findComponentById(ResourceTable.Id_test);
        wowo.addAnimation(text)
                .add(WoWoElevationAnimation.builder().page(0).from(0).to(30).build())
                .add(WoWoElevationAnimation.builder().page(1).from(30).to(60).build())
                .add(WoWoElevationAnimation.builder().page(2).from(60).to(0).build())
                .add(WoWoElevationAnimation.builder().page(3).from(0).to(60).build());

        wowo.setEase(ease);
        wowo.setUseSameEaseBack(useSameEaseTypeBack);
        wowo.ready();

    }
}
