package com.nightonke.wowoviewpagerexample;


import com.nightonke.wowoviewpager.Animation.ViewAnimation;
import com.nightonke.wowoviewpager.Animation.WoWoScaleAnimation;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;

public class WoWoScaleAnimationActivity extends WoWoActivity {

    @Override
    protected int contentViewRes() {
        return ResourceTable.Layout_activity_wowo_position_animation;

    }



    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        Component view = findComponentById(ResourceTable.Id_test);
        ViewAnimation viewAnimation = new ViewAnimation(view);
        viewAnimation.add(WoWoScaleAnimation.builder().page(0).fromXY(1).toXY(0.5).build());
        viewAnimation.add(WoWoScaleAnimation.builder().page(1).fromXY(0.5).toXY(4).build());
        viewAnimation.add(WoWoScaleAnimation.builder().page(2).start(0).end(0.5).fromX(4).toX(1).keepY(4).build());
        viewAnimation.add(WoWoScaleAnimation.builder().page(2).start(0.5).end(1).keepX(1).fromY(4).toY(1).build());
        viewAnimation.add(WoWoScaleAnimation.builder().page(3).start(0).end(0.5).keepX(1).fromY(1).toY(3).build());
        viewAnimation.add(WoWoScaleAnimation.builder().page(3).start(0.5).end(1).fromX(1).toX(3).keepY(3).build());

        wowo.addAnimation(viewAnimation);

        wowo.setEase(ease);
        wowo.setUseSameEaseBack(useSameEaseTypeBack);
        wowo.ready();

    }
}
