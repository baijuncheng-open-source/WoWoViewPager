package com.nightonke.wowoviewpagerexample;


import com.nightonke.wowoviewpager.Animation.ViewAnimation;
import com.nightonke.wowoviewpager.Animation.WoWoStateListColorAnimation;
import com.nightonke.wowoviewpager.Enum.Chameleon;
import com.nightonke.wowoviewpager.WoWoViewPager;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.multimodalinput.event.TouchEvent;

public class WoWoStateListColorAnimationActivity extends WoWoActivity {
    Component test1;
    Component test2;
    @Override
    protected int contentViewRes() {
        return ResourceTable.Layout_activity_wowo_state_list_color_animation;
    }
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        test1 = findComponentById(ResourceTable.Id_test1);
        test2 = findComponentById(ResourceTable.Id_test2);
        addAnimations(test2,Chameleon.HSV);
        addAnimations(test1,Chameleon.RGB);
        test1.setTouchEventListener(new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                switch (touchEvent.getAction()) {
                    case TouchEvent.PRIMARY_POINT_UP:
                        test1.setPressState(false);
                        break;
                    case TouchEvent.PRIMARY_POINT_DOWN:
                        test1.setPressState(true);
                        break;

                }
                return true;
            }
        });
        test2.setTouchEventListener(new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                switch (touchEvent.getAction()) {
                    case TouchEvent.PRIMARY_POINT_UP:
                        test2.setPressState(false);
                        break;
                    case TouchEvent.PRIMARY_POINT_DOWN:
                        test2.setPressState(true);
                        break;

                }
                return true;
            }
        });
        wowo.setOnPageSliding(new WoWoViewPager.OnPageSliding() {
            @Override
            public void onSliding() {

            }
        });
    }

    private void addAnimations(Component view, Chameleon chameleon) {
        ViewAnimation viewAnimation = new ViewAnimation(view);
        viewAnimation.add(WoWoStateListColorAnimation.builder().page(0)
                .from("#ff0000", "#ffff00").to("#ffff00", "#00ff00").chameleon(chameleon).build());
        viewAnimation.add(WoWoStateListColorAnimation.builder().page(1)
                .from("#ffff00", "#00ff00").to("#000000", "#0000ff").chameleon(chameleon).build());
        viewAnimation.add(WoWoStateListColorAnimation.builder().page(2)
                .from("#000000", "#0000ff").to("#ff0000", "#ffff00").chameleon(chameleon).build());
        viewAnimation.add(WoWoStateListColorAnimation.builder().page(3).start(0).end(0.5)
                .from("#ff0000", "#ffff00").to("#000000", "#0000ff").chameleon(chameleon).build());
        viewAnimation.add(WoWoStateListColorAnimation.builder().page(3).start(0.5).end(1)
                .from("#000000", "#0000ff").to("#ff0000", "#ffff00").chameleon(chameleon).build());

        wowo.addAnimation(viewAnimation);

        wowo.setEase(ease);
        wowo.setUseSameEaseBack(useSameEaseTypeBack);
        wowo.ready();
    }
}
