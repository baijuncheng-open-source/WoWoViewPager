package com.nightonke.wowoviewpagerexample;


import com.nightonke.wowoviewpager.Animation.ViewAnimation;
import com.nightonke.wowoviewpager.Animation.WoWoTextViewColorAnimation;
import com.nightonke.wowoviewpager.Enum.Chameleon;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;

public class WoWoTextViewColorAnimationActivity extends WoWoActivity {

    @Override
    protected int contentViewRes() {
        return ResourceTable.Layout_activity_wowo_textview_color_animation;
    }


    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        addAnimations(findComponentById(ResourceTable.Id_test1),Chameleon.RGB);
        addAnimations(findComponentById(ResourceTable.Id_test2),Chameleon.HSV);
    }

    private void addAnimations(Component view, Chameleon chameleon) {
        ViewAnimation viewAnimation = new ViewAnimation(view);
        viewAnimation.add(WoWoTextViewColorAnimation.builder().page(0)
                .from("#ff0000").to("#00ff00").chameleon(chameleon).build());
        viewAnimation.add(WoWoTextViewColorAnimation.builder().page(1)
                .from("#00ff00").to("#0000ff").chameleon(chameleon).build());
        viewAnimation.add(WoWoTextViewColorAnimation.builder().page(2)
                .from("#0000ff").to("#ff0000").chameleon(chameleon).build());
        viewAnimation.add(WoWoTextViewColorAnimation.builder().page(3).start(0).end(0.5)
                .from("#ff0000").to("#000000").chameleon(chameleon).build());
        viewAnimation.add(WoWoTextViewColorAnimation.builder().page(3).start(0.5).end(1)
                .from("#000000").to("#ff0000").chameleon(chameleon).build());
        wowo.addAnimation(viewAnimation);
        wowo.setEase(ease);
        wowo.setUseSameEaseBack(useSameEaseTypeBack);
        wowo.ready();
    }
}
