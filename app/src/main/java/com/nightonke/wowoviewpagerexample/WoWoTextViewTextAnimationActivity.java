package com.nightonke.wowoviewpagerexample;


import com.nightonke.wowoviewpager.Animation.WoWoTextViewTextAnimation;
import com.nightonke.wowoviewpager.Enum.Typewriter;
import com.nightonke.wowoviewpager.Enum.WoWoTypewriter;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;

public class WoWoTextViewTextAnimationActivity extends WoWoActivity {

    @Override
    protected int contentViewRes() {
        return ResourceTable.Layout_activity_wowo_textview_text_animation;
    }


    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        addTextAnimation(findComponentById(ResourceTable.Id_test0), WoWoTypewriter.InsertFromLeft);
        addTextAnimation(findComponentById(ResourceTable.Id_test1), WoWoTypewriter.DeleteThenType);
        addTextAnimation(findComponentById(ResourceTable.Id_test2), WoWoTypewriter.InsertFromRight);

        wowo.setEase(ease);
        wowo.setUseSameEaseBack(useSameEaseTypeBack);
        wowo.ready();
    }

    private void addTextAnimation(Component view, Typewriter typewriter) {
        wowo.addAnimation(view)
                .add(WoWoTextViewTextAnimation.builder().page(0)
                        .from("Nightonke").to("WoWoViewPager").typewriter(typewriter).build())
                .add(WoWoTextViewTextAnimation.builder().page(1)
                        .from("WoWoViewPager").to("").typewriter(typewriter).build())
                .add(WoWoTextViewTextAnimation.builder().page(2)
                        .from("").to("Nightonke").typewriter(typewriter).build())
                .add(WoWoTextViewTextAnimation.builder().page(3).start(0).end(0.5)
                        .from("Nightonke").to("Huang").typewriter(typewriter).build())
                .add(WoWoTextViewTextAnimation.builder().page(3).start(0.5).end(1)
                        .from("Huang").to("Weiping").typewriter(typewriter).build());
    }
}
