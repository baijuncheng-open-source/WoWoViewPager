package com.nightonke.wowoviewpagerexample;


import com.nightonke.wowoviewpager.Animation.ViewAnimation;
import com.nightonke.wowoviewpager.Animation.WoWoTextViewTextSizeAnimation;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;

public class WoWoTextViewTextSizeAnimationActivity extends WoWoActivity {

    @Override
    protected int contentViewRes() {
        return ResourceTable.Layout_activity_wowo_textview_textsize_animation;
    }


    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        Component view = findComponentById(ResourceTable.Id_test);
        ViewAnimation viewAnimation = new ViewAnimation(view);
        viewAnimation.add(WoWoTextViewTextSizeAnimation.builder().page(0)
                .fromSp(50).toSp(20).build());
        viewAnimation.add(WoWoTextViewTextSizeAnimation.builder().page(1)
                .fromSp(20).toSp(60).build());
        viewAnimation.add(WoWoTextViewTextSizeAnimation.builder().page(2)
                .fromSp(60).toSp(5).build());
        viewAnimation.add(WoWoTextViewTextSizeAnimation.builder().page(3).start(0).end(0.5)
                .fromSp(5).toSp(50).build());
        viewAnimation.add(WoWoTextViewTextSizeAnimation.builder().page(3).start(0.5).end(1)
                .fromSp(50).toSp(30).build());
        wowo.addAnimation(viewAnimation);

        wowo.setEase(ease);
        wowo.setUseSameEaseBack(useSameEaseTypeBack);
        wowo.ready();

    }
}
