package com.nightonke.wowoviewpagerexample;


import ohos.agp.utils.Point;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.Optional;

/**
 * Created by Weiping Huang at 23:40 on 2016/3/3
 * For Personal Open Source
 * Contact me at 2584541288@qq.com or nightonke@outlook.com
 * For more projects: https://github.com/Nightonke
 *
 */

class WoWoUtil {

    private static int screenWidth = -1;
    private static int screenHeight = -1;

    /**
     * get the screen width in pixels
     * @param context
     * @return
     */
    public static int getScreenWidth(Context context) {
        if (screenWidth == -1) {
            Optional<Display>
                    display = DisplayManager.getInstance().getDefaultDisplay(context);
            Point pt = new Point();
            display.get().getSize(pt);
            screenWidth = pt.getPointXToInt();
        }
        return screenWidth;
    }

    /**
     * get the screen height in pixels
     * @param context
     * @return
     */
    public static int getScreenHeight(Context context) {
        if (screenHeight == -1) {
            Optional<Display>
                    display = DisplayManager.getInstance().getDefaultDisplay(context);
            Point pt = new Point();
            display.get().getSize(pt);
            screenHeight = pt.getPointYToInt();
        }
        return screenHeight;
    }

    /**
     * dp to px
     * @param mContext
     * @return
     */
    public static int dp2px(int dpValue, Context mContext) {
        float scale = DisplayManager.getInstance().getDefaultDisplay(mContext).get().getAttributes().densityDpi;
        return (int) (dpValue * scale + 0.5F);
    }

    private static WoWoUtil ourInstance = new WoWoUtil();

    public static WoWoUtil getInstance() {
        return ourInstance;
    }

    private WoWoUtil() {
    }
}
