package com.nightonke.wowoviewpagerexample.gif;


import com.nightonke.wowoviewpagerexample.ResourceTable;
import com.nightonke.wowoviewpagerexample.WoWoActivity;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Image;
import ohos.agp.components.PageSlider;
import ohos.agp.components.element.PixelMapElement;
import ohos.global.resource.NotExistException;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;
import java.util.ArrayList;

public class GIFActivity extends WoWoActivity {

    private ImageSource imageSource;
    private ImageSource imageSource1;
    private ImageSource imageSource2;
    private ImageSource imageSource3;
    private ArrayList<ArrayList<PixelMap>> pixelMapListAll = new ArrayList<>();
    int i;

    @Override
    protected int contentViewRes() {
        return ResourceTable.Layout_activity_gif;
    }

    @Override
    protected int fragmentNumber() {
        return 5;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        Image gifView0 = (Image) findComponentById(ResourceTable.Id_gif_0);
        Image gifView1 = (Image) findComponentById(ResourceTable.Id_gif_1);
        Image gifView2 = (Image) findComponentById(ResourceTable.Id_gif_2);
        Image gifView3 = (Image) findComponentById(ResourceTable.Id_gif_3);
        ResourceManager resourceManager = getResourceManager();
        ImageSource.SourceOptions sourceOptions = new ImageSource.SourceOptions();
        ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
        decodingOptions.allowPartialImage = true;
        sourceOptions.formatHint = "image/gif";
        RawFileEntry rawFileEntry = null;
        RawFileEntry rawFileEntry1 = null;
        RawFileEntry rawFileEntry2 = null;
        RawFileEntry rawFileEntry3 = null;
        try {
            rawFileEntry = resourceManager.getRawFileEntry(resourceManager.getMediaPath(ResourceTable.Media_gif_0));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        try {
            rawFileEntry1 = resourceManager.getRawFileEntry(resourceManager.getMediaPath(ResourceTable.Media_gif_1));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        try {
            rawFileEntry2 = resourceManager.getRawFileEntry(resourceManager.getMediaPath(ResourceTable.Media_gif_2));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        try {
            rawFileEntry3 = resourceManager.getRawFileEntry(resourceManager.getMediaPath(ResourceTable.Media_gif_3));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        try {
            imageSource = ImageSource.create(rawFileEntry.openRawFile(), sourceOptions);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            imageSource1 = ImageSource.create(rawFileEntry1.openRawFile(), sourceOptions);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            imageSource2 = ImageSource.create(rawFileEntry2.openRawFile(), sourceOptions);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            imageSource3 = ImageSource.create(rawFileEntry3.openRawFile(), sourceOptions);
        } catch (IOException e) {
            e.printStackTrace();
        }
        pixelMapListAll.clear();
        setData(0, imageSource, decodingOptions);
        setData(1, imageSource1, decodingOptions);
        setData(2, imageSource2, decodingOptions);
        setData(3, imageSource3, decodingOptions);
        wowo.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int position, float v, int i1) {
                if (i1 < 0) {
                    position = position - 1;
                }
                switch (position) {
                    case 0:
                        gifView0.setImageElement(new PixelMapElement(pixelMapListAll.get(0).get((int) (v * (pixelMapListAll.get(0).size() - 1)))));
                        break;
                    case 1:
                        gifView1.setImageElement(new PixelMapElement(pixelMapListAll.get(1).get((int) (v * (pixelMapListAll.get(1).size() - 1)))));
                        break;
                    case 2:
                        gifView2.setImageElement(new PixelMapElement(pixelMapListAll.get(2).get((int) (v * (pixelMapListAll.get(2).size() - 1)))));
                        break;
                    case 3:
                        gifView3.setImageElement(new PixelMapElement(pixelMapListAll.get(3).get((int) (v * (pixelMapListAll.get(3).size() - 1)))));
                        break;
                    default:
                        break;
                }

            }

            @Override
            public void onPageSlideStateChanged(int i) {

            }

            @Override
            public void onPageChosen(int i) {

            }
        });
//
//        wowo.addAnimation(gifView0)
//                .add(WoWoInterfaceAnimation.builder().page(0).implementedBy(gifView0).build());
//
//        wowo.addAnimation(gifView1)
//                .add(WoWoInterfaceAnimation.builder().page(1).implementedBy(gifView1).build());
//
//        wowo.addAnimation(gifView2)
//                .add(WoWoInterfaceAnimation.builder().page(2).implementedBy(gifView2).build());
//
//        wowo.addAnimation(gifView3)
//                .add(WoWoInterfaceAnimation.builder().page(3).implementedBy(gifView3).build());
//
//        wowo.ready();
    }

    private void setData(int position, ImageSource imageSource, ImageSource.DecodingOptions decodingOptions) {
        ArrayList<PixelMap> pixelMapList = new ArrayList<>();
        if (imageSource != null) {
            i = 0;
            try {
                while (imageSource.createPixelmap(i, decodingOptions) != null) {
                    pixelMapList.add(imageSource.createPixelmap(i, decodingOptions));
                    i++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        pixelMapListAll.add(pixelMapList);
    }


}
