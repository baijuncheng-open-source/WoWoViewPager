package com.nightonke.wowoviewpagerexample.slice;

import com.nightonke.wowoviewpagerexample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.BaseDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.dialog.PopupDialog;
import ohos.utils.net.Uri;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private Integer[] resourceIds = new Integer[]{ResourceTable.Id_more, ResourceTable.Id_wowo_position_animation, ResourceTable.Id_wowo_translation_animation, ResourceTable.Id_wowo_scale_animation, ResourceTable.Id_wowo_alpha_animation
            , ResourceTable.Id_wowo_rotation_animation
            , ResourceTable.Id_wowo_elevation_animation, ResourceTable.Id_wowo_text_view_text_size_animation, ResourceTable.Id_wowo_text_view_color_animation, ResourceTable.Id_wowo_text_view_text_animation, ResourceTable.Id_wowo_background_color_animation,
            ResourceTable.Id_wowo_drawable_color_animation, ResourceTable.Id_wowo_layer_list_color_animation, ResourceTable.Id_wowo_state_list_color_animation, ResourceTable.Id_wowo_path_animation
            , ResourceTable.Id_gearbox, ResourceTable.Id_static_view_pager, ResourceTable.Id_auto_scroll, ResourceTable.Id_direction, ResourceTable.Id_svg_expansibility
            , ResourceTable.Id_gif_expansibility, ResourceTable.Id_custom_animation, ResourceTable.Id_guide_page_1, ResourceTable.Id_guide_page_2};
    private PopupDialog mMenuDialog;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initView();

    }

    private void initView() {
        setThisAsOnClickListener(resourceIds);

    }

    private void setThisAsOnClickListener(Integer[] resourceIds) {
        for (int res : resourceIds) findComponentById(res).setClickedListener(this);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onClick(Component component) {
        Intent intent = new Intent();
        Operation operation;
        switch (component.getId()) {
            case ResourceTable.Id_more:
                onCreateOptionsMenu();
                return;
            case ResourceTable.Id_wowo_position_animation:
                intent.setParam("AnimationType", "WoWoPositionAnimation");
                break;
            case ResourceTable.Id_wowo_translation_animation:
                intent.setParam("AnimationType", "WoWoTranslationAnimation");
                break;
            case ResourceTable.Id_wowo_scale_animation:
                intent.setParam("AnimationType", "WoWoScaleAnimation");
                break;
            case ResourceTable.Id_wowo_alpha_animation:
                intent.setParam("AnimationType", "WoWoAlphaAnimation");
                break;
            case ResourceTable.Id_wowo_rotation_animation:
                intent.setParam("AnimationType", "WoWoRotationAnimation");
                break;
            case ResourceTable.Id_wowo_elevation_animation:
                intent.setParam("AnimationType", "WoWoElevationAnimation");
                break;
            case ResourceTable.Id_wowo_text_view_text_size_animation:
                intent.setParam("AnimationType", "WoWoTextViewTextSizeAnimation");
                break;
            case ResourceTable.Id_wowo_text_view_color_animation:
                intent.setParam("AnimationType", "WoWoTextViewColorAnimation");
                break;
            case ResourceTable.Id_wowo_text_view_text_animation:
                intent.setParam("AnimationType", "WoWoTextViewTextAnimation");
                break;
            case ResourceTable.Id_wowo_background_color_animation:
                intent.setParam("AnimationType", "WoWoBackgroundColorAnimation");
                break;
            case ResourceTable.Id_wowo_drawable_color_animation:
                intent.setParam("AnimationType", "WoWoShapeColorAnimation");
                break;
            case ResourceTable.Id_wowo_layer_list_color_animation:
                intent.setParam("AnimationType", "WoWoLayerListColorAnimation");
                break;
            case ResourceTable.Id_wowo_state_list_color_animation:
                intent.setParam("AnimationType", "WoWoStateListColorAnimation");
                break;
            case ResourceTable.Id_wowo_path_animation:
                intent.setParam("AnimationType", "WoWoPathAnimation");
                break;
            case ResourceTable.Id_gearbox:
                operation = new Intent.OperationBuilder()
                        .withBundleName("com.nightonke.wowoviewpagerexample")
                        .withAbilityName("com.nightonke.wowoviewpagerexample.GearboxActivity")
                        .build();
                intent.setOperation(operation);
                getAbility().startAbility(intent);
                return;
            case ResourceTable.Id_static_view_pager:
                operation = new Intent.OperationBuilder()
                        .withBundleName("com.nightonke.wowoviewpagerexample")
                        .withAbilityName("com.nightonke.wowoviewpagerexample.StaticActivity")
                        .build();
                intent.setOperation(operation);
                getAbility().startAbility(intent);
                return;
            case ResourceTable.Id_auto_scroll:
                operation = new Intent.OperationBuilder()
                        .withBundleName("com.nightonke.wowoviewpagerexample")
                        .withAbilityName("com.nightonke.wowoviewpagerexample.AutoScrollActivity")
                        .build();
                intent.setOperation(operation);
                getAbility().startAbility(intent);
                return;
            case ResourceTable.Id_direction:
                operation = new Intent.OperationBuilder()
                        .withBundleName("com.nightonke.wowoviewpagerexample")
                        .withAbilityName("com.nightonke.wowoviewpagerexample.DirectionActivity")
                        .build();
                intent.setOperation(operation);
                getAbility().startAbility(intent);
                return;
            case ResourceTable.Id_svg_expansibility:
                operation = new Intent.OperationBuilder()
                        .withBundleName("com.nightonke.wowoviewpagerexample")
                        .withAbilityName("com.nightonke.wowoviewpagerexample.svg.SVGActivity")
                        .build();
                intent.setOperation(operation);
                getAbility().startAbility(intent);
                return;
            case ResourceTable.Id_gif_expansibility:
                operation = new Intent.OperationBuilder()
                        .withBundleName("com.nightonke.wowoviewpagerexample")
                        .withAbilityName("com.nightonke.wowoviewpagerexample.gif.GIFActivity")
                        .build();
                intent.setOperation(operation);
                getAbility().startAbility(intent);
                return;
            case ResourceTable.Id_custom_animation:
                intent.setParam("AnimationType", "CustomAnimation");
                break;
            case ResourceTable.Id_guide_page_1:
                operation = new Intent.OperationBuilder()
                        .withBundleName("com.nightonke.wowoviewpagerexample")
                        .withAbilityName("com.nightonke.wowoviewpagerexample.GuidePageActivity1")
                        .build();
                intent.setOperation(operation);
                getAbility().startAbility(intent);
                return;
            case ResourceTable.Id_guide_page_2:
                operation = new Intent.OperationBuilder()
                        .withBundleName("com.nightonke.wowoviewpagerexample")
                        .withAbilityName("com.nightonke.wowoviewpagerexample.GuidePageActivity2")
                        .build();
                intent.setOperation(operation);
                getAbility().startAbility(intent);
                return;
        }
        operation = new Intent.OperationBuilder()
                .withBundleName("com.nightonke.wowoviewpagerexample")
                .withAbilityName("com.nightonke.wowoviewpagerexample.SetEaseTypeActivity")
                .build();
        intent.setOperation(operation);
        getAbility().startAbility(intent);

    }

    public void onCreateOptionsMenu() {
        if (mMenuDialog != null) {
            return;
        }
        Component menu = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_menu, null, false);
        Text github = (Text) menu.findComponentById(ResourceTable.Id_github);
        Text developer = (Text) menu.findComponentById(ResourceTable.Id_developer);
        github.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent i = new Intent();
                i.setUri(Uri.parse("https://github.com/linweixh/WoWoViewPager"));
                startAbility(i);
                mMenuDialog.remove();
                mMenuDialog = null; //remove()失效
            }
        });
        developer.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent i = new Intent();
                i.setUri(Uri.parse("https://github.com/linweixh/WoWoViewPager"));
                startAbility(i);
                mMenuDialog.remove();
                mMenuDialog = null; //remove()失效
            }
        });

        mMenuDialog = new PopupDialog(getAbility(), findComponentById(ResourceTable.Id_more));
        mMenuDialog.setCustomComponent(menu);
        mMenuDialog.showOnCertainPosition(LayoutAlignment.TOP, 280, 100);
        mMenuDialog.setDialogListener(new BaseDialog.DialogListener() {
            @Override
            public boolean isTouchOutside() {
                mMenuDialog.remove();
                mMenuDialog = null; //remove()失效
                return false;
            }
        });
        mMenuDialog.registerRemoveCallback(new BaseDialog.RemoveCallback() {
            @Override
            public void onRemove(IDialog iDialog) {
                mMenuDialog = null; //remove()失效
            }
        });
    }
}
