package com.nightonke.wowoviewpager.Animation;


import com.nightonke.wowoviewpager.Enum.Chameleon;
import com.nightonke.wowoviewpager.Enum.TimeInterpolator;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.element.ShapeElement;

/**
 * Created by Weiping Huang at 02:05 on 2017/3/30
 * For Personal Open Source
 * Contact me at 2584541288@qq.com or nightonke@outlook.com
 * For more projects: https://github.com/Nightonke
 *
 * Animation to change background color with the method {}
 */

public class WoWoBackgroundColorAnimation extends SingleColorPageAnimation {

    /**
     * Construct a page animation for a single color translation.
     *
     * @param page                The animation will be played when the (page + 1) page is starting to show.
     * @param startOffset         The animation only plays when the offset of page is large than startOffset.
     * @param endOffset           The animation only plays when the offset of page is less than endOffset.
     * @param ease                The ease type of the animation.
     * @param interpolator        Custom time interpolator.
     * @param useSameEaseEnumBack Whether use the same ease type of animation when swiping back the view-pager.
     * @param fromColor           The starting-color.
     * @param toColor             The ending-color.
     * @param chameleon           The color-changing-type. Check {@link Chameleon}
     */
    private WoWoBackgroundColorAnimation(int page, float startOffset, float endOffset, int ease, TimeInterpolator interpolator, boolean useSameEaseEnumBack, Integer fromColor, Integer toColor, Chameleon chameleon) {
        super(page, startOffset, endOffset, ease, interpolator, useSameEaseEnumBack, fromColor, toColor, chameleon);
    }

    @Override
    protected void toStartState(Component view) {
        ShapeElement shapeElement = new ShapeElement();
//        shapeElement.setRgbColor(new RgbColor(new Color(fromColor).getValue()));
        shapeElement.setRgbColor(new RgbColor(RgbColor.fromArgbInt(fromColor)));
        view.setBackground(shapeElement);
    }

    @Override
    protected void toMiddleState(Component view, float offset) {
        ShapeElement shapeElement = new ShapeElement();
//        shapeElement.setRgbColor(new RgbColor(new Color(middleColor(chameleon, offset)).getValue()));
        shapeElement.setRgbColor(new RgbColor(RgbColor.fromArgbInt(middleColor(chameleon, offset))));
        view.setBackground(shapeElement);
    }

    @Override
    protected void toEndState(Component view) {
        ShapeElement shapeElement = new ShapeElement();
//        shapeElement.setRgbColor(new RgbColor(new Color(toColor).getValue()));
        shapeElement.setRgbColor(new RgbColor(RgbColor.fromArgbInt(toColor)));
        view.setBackground(shapeElement);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder extends SingleColorPageAnimation.Builder<WoWoBackgroundColorAnimation.Builder> {

        public WoWoBackgroundColorAnimation build() {
            checkUninitializedAttributes();
            return new WoWoBackgroundColorAnimation(page, startOffset, endOffset, ease, interpolator, useSameEaseEnumBack, fromColor, toColor, chameleon);
        }
    }
}
