package com.nightonke.wowoviewpager.Animation;


import com.nightonke.wowoviewpager.Enum.Chameleon;
import com.nightonke.wowoviewpager.Enum.LayerDrawable;
import com.nightonke.wowoviewpager.Enum.TimeInterpolator;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;

/**
 * Created by Weiping Huang at 16:05 on 2017/3/30
 * For Personal Open Source
 * Contact me at 2584541288@qq.com or nightonke@outlook.com
 * For more projects: https://github.com/Nightonke
 * <p>
 * Animation to change multi-colors for state-list {@link LayerDrawable}
 */

public class WoWoLayerListColorAnimation extends MultiColorPageAnimation {

    /**
     * Construct a page animation for multi-colors translation.
     *
     * @param page                The animation will be played when the (page + 1) page is starting to show.
     * @param startOffset         The animation only plays when the offset of page is large than startOffset.
     * @param endOffset           The animation only plays when the offset of page is less than endOffset.
     * @param ease                The ease type of the animation.
     * @param interpolator        Custom time interpolator.
     * @param useSameEaseEnumBack Whether use the same ease type of animation when swiping back the view-pager.
     * @param fromColors          The starting-colors.
     * @param toColors            The ending-colors.
     * @param chameleon           The color-changing-type. Check {@link Chameleon}
     */
    private WoWoLayerListColorAnimation(int page, float startOffset, float endOffset, int ease, TimeInterpolator interpolator, boolean useSameEaseEnumBack, int[] fromColors, int[] toColors, Chameleon chameleon) {
        super(page, startOffset, endOffset, ease, interpolator, useSameEaseEnumBack, fromColors, toColors, chameleon);
    }

    @Override
    protected void toStartState(Component view) {
        setColors(view, fromColors);
    }

    @Override
    protected void toMiddleState(Component view, float offset) {
        setColors(view, middleColors(chameleon, offset));
    }

    @Override
    protected void toEndState(Component view) {
        setColors(view, toColors);
    }

    private void setColors(Component view, int[] colors) {
        Element drawable = view.getBackgroundElement();
        if (drawable instanceof ShapeElement) {
            ShapeElement layerDrawable = (ShapeElement) drawable;
            for (int i = 0; i < colors.length; i++) {
                layerDrawable.setRgbColor(new RgbColor(RgbColor.fromArgbInt(colors[i])));
            }
            view.setBackground(layerDrawable);
//            for (int i = 0; i < colors.length; i++) if (layerDrawable.getDrawable(i) instanceof GradientDrawable) ((GradientDrawable) layerDrawable.getDrawable(i)).setColor(colors[i]);
        } else {
//            Log.w(TAG, "Drawable of view must be LayerDrawable in WoWoLayerListColorAnimation");
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder extends MultiColorPageAnimation.Builder<WoWoLayerListColorAnimation.Builder> {

        public WoWoLayerListColorAnimation build() {
            checkUninitializedAttributes();
            return new WoWoLayerListColorAnimation(page, startOffset, endOffset, ease, interpolator, useSameEaseEnumBack, fromColors, toColors, chameleon);
        }
    }
}
