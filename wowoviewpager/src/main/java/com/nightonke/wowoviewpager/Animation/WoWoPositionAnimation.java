package com.nightonke.wowoviewpager.Animation;


import com.nightonke.wowoviewpager.Enum.TimeInterpolator;
import ohos.agp.components.Component;

/**
 * Created by Weiping Huang at 18:13 on 2017/3/29
 * For Personal Open Source
 * Contact me at 2584541288@qq.com or nightonke@outlook.com
 * For more projects: https://github.com/Nightonke
 *
 * Animation to change 2D-position of a view.
 */

public class WoWoPositionAnimation extends XYPageAnimation {

    private WoWoPositionAnimation(int page, float startOffset, float endOffset, int ease, TimeInterpolator interpolator, boolean useSameEaseEnumBack, float fromX, float fromY, float toX, float toY) {
        super(page, startOffset, endOffset, ease, interpolator, useSameEaseEnumBack, fromX, fromY, toX, toY);
    }

    @Override
    protected void toStartState(Component view) {
        view.setContentPositionX(fromX);
        view.setContentPositionY(fromY);
    }

    @Override
    protected void toMiddleState(Component view, float offset) {
        view.setContentPositionX((float) (fromX + (toX - fromX) * offset));
        view.setContentPositionY((float) (fromY + (toY - fromY) * offset));
    }

    @Override
    protected void toEndState(Component view) {
        view.setContentPositionX(toX);
        view.setContentPositionY(toY);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder extends XYPageAnimation.Builder<WoWoPositionAnimation.Builder> {

        public WoWoPositionAnimation build() {
            checkUninitializedAttributes();
            return new WoWoPositionAnimation(page, startOffset, endOffset, ease, interpolator, useSameEaseEnumBack, fromX, fromY, toX, toY);
        }
    }
}
