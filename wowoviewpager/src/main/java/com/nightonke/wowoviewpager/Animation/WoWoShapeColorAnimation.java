package com.nightonke.wowoviewpager.Animation;


import com.nightonke.wowoviewpager.Enum.Chameleon;
import com.nightonke.wowoviewpager.Enum.TimeInterpolator;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;


/**
 * Created by Weiping Huang at 10:49 on 2017/3/30
 * For Personal Open Source
 * Contact me at 2584541288@qq.com or nightonke@outlook.com
 * For more projects: https://github.com/Nightonke
 */

public class WoWoShapeColorAnimation extends SingleColorPageAnimation {

    private WoWoShapeColorAnimation(int page, float startOffset, float endOffset, int ease, TimeInterpolator interpolator, boolean useSameEaseEnumBack, Integer fromColor, Integer toColor, Chameleon chameleon) {
        super(page, startOffset, endOffset, ease, interpolator, useSameEaseEnumBack, fromColor, toColor, chameleon);
    }

    @Override
    protected void toStartState(Component view) {
        setColor(view, fromColor);
    }

    @Override
    protected void toMiddleState(Component view, float offset) {
        setColor(view, middleColor(chameleon, offset));
    }

    @Override
    protected void toEndState(Component view) {
        setColor(view, toColor);
    }

    private void setColor(Component view, int color) {
        Element drawable = view.getBackgroundElement();
        if (drawable instanceof ShapeElement) {
            ShapeElement layerDrawable = (ShapeElement) drawable;
            layerDrawable.setRgbColor(new RgbColor(RgbColor.fromArgbInt(color)));
            view.setBackground(layerDrawable);
//            Log.w(TAG, "Drawable of view must be LayerDrawable in WoWoLayerListColorAnimation");
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder extends SingleColorPageAnimation.Builder<WoWoShapeColorAnimation.Builder> {

        public WoWoShapeColorAnimation build() {
            checkUninitializedAttributes();
            return new WoWoShapeColorAnimation(page, startOffset, endOffset, ease, interpolator, useSameEaseEnumBack, fromColor, toColor, chameleon);
        }
    }
}
