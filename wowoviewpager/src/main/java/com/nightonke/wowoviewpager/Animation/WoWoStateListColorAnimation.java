package com.nightonke.wowoviewpager.Animation;


import com.nightonke.wowoviewpager.Enum.Chameleon;
import com.nightonke.wowoviewpager.Enum.TimeInterpolator;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;


/**
 * Created by Weiping Huang at 12:25 on 2017/3/30
 * For Personal Open Source
 * Contact me at 2584541288@qq.com or nightonke@outlook.com
 * For more projects: https://github.com/Nightonke
 */

public class WoWoStateListColorAnimation extends MultiColorPageAnimation {

    /**
     * Construct a page animation for multi-colors translation.
     *
     * @param page                The animation will be played when the (page + 1) page is starting to show.
     * @param startOffset         The animation only plays when the offset of page is large than startOffset.
     * @param endOffset           The animation only plays when the offset of page is less than endOffset.
     * @param ease                The ease type of the animation.
     * @param interpolator        Custom time interpolator.
     * @param useSameEaseEnumBack Whether use the same ease type of animation when swiping back the view-pager.
     * @param fromColors          The starting-colors.
     * @param toColors            The ending-colors.
     * @param chameleon           The color-changing-type. Check {@link Chameleon}
     */
    private WoWoStateListColorAnimation(int page, float startOffset, float endOffset, int ease, TimeInterpolator interpolator, boolean useSameEaseEnumBack, int[] fromColors, int[] toColors, Chameleon chameleon) {
        super(page, startOffset, endOffset, ease, interpolator, useSameEaseEnumBack, fromColors, toColors, chameleon);
    }

    @Override
    protected void toStartState(Component view) {
        setColors(view, fromColors);
    }

    @Override
    protected void toMiddleState(Component view, float offset) {
        setColors(view, middleColors(chameleon, offset));
    }

    @Override
    protected void toEndState(Component view) {
        setColors(view, toColors);
    }

    private void setColors(Component view, int[] colors) {
        Element drawable = view.getBackgroundElement();
        if (drawable instanceof StateElement) {
            StateElement layerDrawable = (StateElement) drawable;
            for (int i = 0; i < colors.length; i++) {
                ShapeElement stateElement = (ShapeElement) layerDrawable.getStateElement(i);
                stateElement.setRgbColor(new RgbColor(RgbColor.fromArgbInt(colors[i])));
                if (view.isPressed()) {
                    if (i == 1) {
                        view.setBackground(stateElement);
                    }
                } else {
                    if (i == 0) {
                        view.setBackground(stateElement);
                    }
                }
            }
            view.setBackground(layerDrawable);
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder extends MultiColorPageAnimation.Builder<WoWoStateListColorAnimation.Builder> {

        public WoWoStateListColorAnimation build() {
            checkUninitializedAttributes();
            return new WoWoStateListColorAnimation(page, startOffset, endOffset, ease, interpolator, useSameEaseEnumBack, fromColors, toColors, chameleon);
        }
    }
}
