package com.nightonke.wowoviewpager.Enum;

public class EdgeEffectCompatLollipop {
    public static boolean onPull(Object edgeEffect, float deltaDistance, float displacement) {
        ((EdgeEffect) edgeEffect).onPull(deltaDistance, displacement);
        return true;
    }
}
