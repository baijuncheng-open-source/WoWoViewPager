package com.nightonke.wowoviewpager.Enum;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.ColorFilter;
import ohos.agp.utils.Rect;
import ohos.global.icu.text.Normalizer2;
import ohos.global.resource.Resource;
import ohos.global.resource.solidxml.Theme;
import ohos.org.xml.sax.Parser;


public class LayerDrawable extends ShapeElement  {
    public static final int INSET_UNDEFINED = -2147483648;
    public static final int PADDING_MODE_NEST = 0;
    public static final int PADDING_MODE_STACK = 1;

    public LayerDrawable(ShapeElement[] layers) {
        throw new RuntimeException("Stub!");
    }

    public void inflate(Resource r, Parser parser, AttrSet attrs, Theme theme) throws Exception {
        throw new RuntimeException("Stub!");
    }

    public void applyTheme(Theme t) {
        throw new RuntimeException("Stub!");
    }

    public boolean canApplyTheme() {
        throw new RuntimeException("Stub!");
    }

    public int addLayer(ShapeElement dr) {
        throw new RuntimeException("Stub!");
    }

    public ShapeElement findDrawableByLayerId(int id) {
        throw new RuntimeException("Stub!");
    }

    public void setId(int index, int id) {
        throw new RuntimeException("Stub!");
    }

    public int getId(int index) {
        throw new RuntimeException("Stub!");
    }

    public int getNumberOfLayers() {
        throw new RuntimeException("Stub!");
    }

    public boolean setDrawableByLayerId(int id, ShapeElement drawable) {
        throw new RuntimeException("Stub!");
    }

    public int findIndexByLayerId(int id) {
        throw new RuntimeException("Stub!");
    }

    public void setDrawable(int index, ShapeElement drawable) {
        throw new RuntimeException("Stub!");
    }

    public ShapeElement getDrawable(int index) {
        throw new RuntimeException("Stub!");
    }

    public void setLayerSize(int index, int w, int h) {
        throw new RuntimeException("Stub!");
    }

    public void setLayerWidth(int index, int w) {
        throw new RuntimeException("Stub!");
    }

    public int getLayerWidth(int index) {
        throw new RuntimeException("Stub!");
    }

    public void setLayerHeight(int index, int h) {
        throw new RuntimeException("Stub!");
    }

    public int getLayerHeight(int index) {
        throw new RuntimeException("Stub!");
    }

    public void setLayerGravity(int index, int gravity) {
        throw new RuntimeException("Stub!");
    }

    public int getLayerGravity(int index) {
        throw new RuntimeException("Stub!");
    }

    public void setLayerInset(int index, int l, int t, int r, int b) {
        throw new RuntimeException("Stub!");
    }

    public void setLayerInsetRelative(int index, int s, int t, int e, int b) {
        throw new RuntimeException("Stub!");
    }

    public void setLayerInsetLeft(int index, int l) {
        throw new RuntimeException("Stub!");
    }

    public int getLayerInsetLeft(int index) {
        throw new RuntimeException("Stub!");
    }

    public void setLayerInsetRight(int index, int r) {
        throw new RuntimeException("Stub!");
    }

    public int getLayerInsetRight(int index) {
        throw new RuntimeException("Stub!");
    }

    public void setLayerInsetTop(int index, int t) {
        throw new RuntimeException("Stub!");
    }

    public int getLayerInsetTop(int index) {
        throw new RuntimeException("Stub!");
    }

    public void setLayerInsetBottom(int index, int b) {
        throw new RuntimeException("Stub!");
    }

    public int getLayerInsetBottom(int index) {
        throw new RuntimeException("Stub!");
    }

    public void setLayerInsetStart(int index, int s) {
        throw new RuntimeException("Stub!");
    }

    public int getLayerInsetStart(int index) {
        throw new RuntimeException("Stub!");
    }

    public void setLayerInsetEnd(int index, int e) {
        throw new RuntimeException("Stub!");
    }

    public int getLayerInsetEnd(int index) {
        throw new RuntimeException("Stub!");
    }

    public void setPaddingMode(int mode) {
        throw new RuntimeException("Stub!");
    }

    public int getPaddingMode() {
        throw new RuntimeException("Stub!");
    }

    public void invalidateDrawable(ShapeElement who) {
        throw new RuntimeException("Stub!");
    }

    public void scheduleDrawable(ShapeElement who, Runnable what, long when) {
        throw new RuntimeException("Stub!");
    }

    public void unscheduleDrawable(ShapeElement who, Runnable what) {
        throw new RuntimeException("Stub!");
    }

    public void draw(Canvas canvas) {
        throw new RuntimeException("Stub!");
    }

    public int getChangingConfigurations() {
        throw new RuntimeException("Stub!");
    }

    public boolean getPadding(Rect padding) {
        throw new RuntimeException("Stub!");
    }

    public void setPadding(int left, int top, int right, int bottom) {
        throw new RuntimeException("Stub!");
    }

    public void setPaddingRelative(int start, int top, int end, int bottom) {
        throw new RuntimeException("Stub!");
    }

    public int getLeftPadding() {
        throw new RuntimeException("Stub!");
    }

    public int getRightPadding() {
        throw new RuntimeException("Stub!");
    }

    public int getStartPadding() {
        throw new RuntimeException("Stub!");
    }

    public int getEndPadding() {
        throw new RuntimeException("Stub!");
    }

    public int getTopPadding() {
        throw new RuntimeException("Stub!");
    }

    public int getBottomPadding() {
        throw new RuntimeException("Stub!");
    }


    public void setHotspot(float x, float y) {
        throw new RuntimeException("Stub!");
    }

    public void setHotspotBounds(int left, int top, int right, int bottom) {
        throw new RuntimeException("Stub!");
    }

    public void getHotspotBounds(Rect outRect) {
        throw new RuntimeException("Stub!");
    }

    public boolean setVisible(boolean visible, boolean restart) {
        throw new RuntimeException("Stub!");
    }

    public void setDither(boolean dither) {
        throw new RuntimeException("Stub!");
    }

    public void setAlpha(int alpha) {
        throw new RuntimeException("Stub!");
    }

    public int getAlpha() {
        throw new RuntimeException("Stub!");
    }

    public void setColorFilter(ColorFilter colorFilter) {
        throw new RuntimeException("Stub!");
    }


    public void setTintMode(Normalizer2.Mode tintMode) {
        throw new RuntimeException("Stub!");
    }

    public void setOpacity(int opacity) {
        throw new RuntimeException("Stub!");
    }

    public int getOpacity() {
        throw new RuntimeException("Stub!");
    }

    public void setAutoMirrored(boolean mirrored) {
        throw new RuntimeException("Stub!");
    }

    public boolean isAutoMirrored() {
        throw new RuntimeException("Stub!");
    }

    public void jumpToCurrentState() {
        throw new RuntimeException("Stub!");
    }

    public boolean isStateful() {
        throw new RuntimeException("Stub!");
    }

    protected boolean onStateChange(int[] state) {
        throw new RuntimeException("Stub!");
    }

    protected boolean onLevelChange(int level) {
        throw new RuntimeException("Stub!");
    }

    protected void onBoundsChange(Rect bounds) {
        throw new RuntimeException("Stub!");
    }

    public int getIntrinsicWidth() {
        throw new RuntimeException("Stub!");
    }

    public int getIntrinsicHeight() {
        throw new RuntimeException("Stub!");
    }

    public ConstantState getConstantState() {
        throw new RuntimeException("Stub!");
    }

    public ShapeElement mutate() {
        throw new RuntimeException("Stub!");
    }

    public boolean onLayoutDirectionChanged(int layoutDirection) {
        throw new RuntimeException("Stub!");
    }

    public interface Callback {
        void invalidateDrawable(ShapeElement var1);

        void scheduleDrawable(ShapeElement var1, Runnable var2, long var3);

        void unscheduleDrawable(ShapeElement var1, Runnable var2);
    }
}

