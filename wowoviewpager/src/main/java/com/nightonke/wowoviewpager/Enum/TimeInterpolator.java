package com.nightonke.wowoviewpager.Enum;

public interface TimeInterpolator {
    float getInterpolation(float input);
}
