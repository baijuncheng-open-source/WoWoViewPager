package com.nightonke.wowoviewpager;

import com.nightonke.wowoviewpager.Enum.Interpolator;
import ohos.agp.components.ScrollHelper;
import ohos.app.Context;

/**
 * Created by Weiping Huang at 14:01 on 2016/3/7
 * For Personal Open Source
 * Contact me at 2584541288@qq.com or nightonke@outlook.com
 * For more projects: https://github.com/Nightonke
 *
 */

class WoWoScroller extends ScrollHelper {

    private int duration = -1;

    public WoWoScroller(Context context) {
        super();
    }

    WoWoScroller(Context context, Interpolator interpolator) {
        super();
    }

    public WoWoScroller(Context context, Interpolator interpolator, boolean flywheel) {
        super();
    }

    public WoWoScroller(int duration) {
        this.duration = duration;
    }

    @Override
    public void startScroll(int startX, int startY, int dx, int dy) {
        // Ignore received duration, use fixed one instead
        startScroll(startX, startY, dx, dy);
    }

    void setDuration(int duration) {
        this.duration = duration;
    }
}
