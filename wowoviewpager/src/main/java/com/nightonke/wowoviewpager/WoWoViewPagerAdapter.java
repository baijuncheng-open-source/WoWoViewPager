package com.nightonke.wowoviewpager;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Weiping Huang at 15:14 on 2016/3/3
 * For Personal Open Source
 * Contact me at 2584541288@qq.com or nightonke@outlook.com
 * For more projects: https://github.com/Nightonke
 */

public class WoWoViewPagerAdapter extends PageSliderProvider {

    private ArrayList<WoWoViewPagerFragment> mFragments;
    private int fragmentsNumber;

    /**
     * fragments' color
     */
    private Integer colorRes = null;
    private Integer color = null;
    private ArrayList<Integer> colorsRes = null;
    private ArrayList<Integer> colors = null;

    public WoWoViewPagerAdapter(ArrayList<WoWoViewPagerFragment> fragments) {
        super();
        mFragments = fragments;
    }

    public int getFragmentsNumber() {
        return fragmentsNumber;
    }

    public void setFragmentsNumber(int fragmentsNumber) {
        this.fragmentsNumber = fragmentsNumber;
    }


    @Override
    public int getCount() {
        return fragmentsNumber;
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int position) {

        WoWoViewPagerFragment fragment = null;

        if (position < mFragments.size()) fragment = mFragments.get(position);

        if (fragment == null) {
            fragment = new WoWoViewPagerFragment(fragment.getContext());
            if (colorRes != null) {
                // the resource of color of all fragments has been set
                fragment.setColorRes(colorRes);
            } else {
                if (color != null) {
                    // the color of all fragments has been set
                    fragment.setColor(color);
                } else {
                    if (colors != null) {
                        if (position < 0 || position >= colors.size()) {
                            // out of index
                            fragment.setColor(colors.get(position));
                        } else {
                            fragment.setColor(Color.TRANSPARENT.getValue());
                        }
                    } else {
                        if (colorsRes != null) {
                            if (position < 0 || position >= colorsRes.size()) {
                                // out of index
                                fragment.setColor(Color.TRANSPARENT.getValue());
                            } else {
                                fragment.setColorRes(colorsRes.get(position));
                            }
                        } else {
                            fragment.setColor(Color.TRANSPARENT.getValue());
                        }
                    }
                }
            }
        }
        componentContainer.addComponent(fragment);
        mFragments.add(fragment);
        return fragment;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        componentContainer.removeComponent(mFragments.get(i));
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return component == o;
    }

    public int getColorRes() {
        return colorRes;
    }

    /**
     * set resource of color of fragments
     *
     * @param colorRes resource of color of fragments
     */
    public void setColorRes(int colorRes) {
        this.colorRes = colorRes;
        colorsRes = null;
        colors = null;
        color = null;
    }

    public Integer getColor() {
        return color;
    }

    /**
     * set color of fragments
     *
     * @param color color of fragments
     */
    public void setColor(Integer color) {
        this.color = color;
        colorRes = null;
        colors = null;
        colorsRes = null;
    }

    /**
     * set resources of colors of fragments
     *
     * @return resources of colors of fragments
     */
    public ArrayList<Integer> getColorsRes() {
        return colorsRes;
    }

    public void setColorsRes(Integer... colorsRes) {
        setColorsRes(new ArrayList<>(Arrays.asList(colorsRes)));
    }

    /**
     * set resources of colors of fragments
     *
     * @param colorsRes resources of colors of fragments
     */
    public void setColorsRes(ArrayList<Integer> colorsRes) {
        this.colorsRes = colorsRes;
        colors = null;
        color = null;
        colorRes = null;
    }

    public ArrayList<Integer> getColors() {
        return colors;
    }

    /**
     * set colors of fragments
     *
     * @param colors colors
     */
    public void setColors(Integer... colors) {
        setColors(new ArrayList<>(Arrays.asList(colors)));
    }

    /**
     * set colors of fragments
     *
     * @param colors colors
     */
    public void setColors(ArrayList<Integer> colors) {
        this.colors = colors;
        colorRes = null;
        color = null;
        colorsRes = null;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private int fragmentNumber = 0;
        private Integer colorRes = null;
        private Integer color = null;
        private ArrayList<Integer> colorsRes = null;
        private ArrayList<Integer> colors = null;

        public Builder count(int fragmentNumber) {
            this.fragmentNumber = fragmentNumber;
            return this;
        }

        public Builder colorRes(Integer colorRes) {
            this.colorRes = colorRes;
            return this;
        }

        public Builder color(Integer color) {
            this.color = color;
            return this;
        }

        public Builder colorsRes(ArrayList<Integer> colorsRes) {
            this.colorsRes = colorsRes;
            return this;
        }

        public Builder colorsRes(Integer... colorsRes) {
            return colorsRes(new ArrayList<>(Arrays.asList(colorsRes)));
        }

        public Builder colors(ArrayList<Integer> colors) {
            this.colors = colors;
            return this;
        }

        public Builder colors(Integer... colors) {
            return colors(new ArrayList<>(Arrays.asList(colors)));
        }

        public WoWoViewPagerAdapter build(Context context) {
            ArrayList<WoWoViewPagerFragment> viewPagerTests = new ArrayList<>();
            for (int i = 0; i < fragmentNumber; i++) {
                WoWoViewPagerFragment woWoViewPagerFragment = new WoWoViewPagerFragment(context);
                ShapeElement shapeElement = new ShapeElement();
                int color = 0;
                try {
                    color = context.getResourceManager().getElement(colorsRes.get(i)).getColor();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NotExistException e) {
                    e.printStackTrace();
                } catch (WrongTypeException e) {
                    e.printStackTrace();
                }
                shapeElement.setRgbColor(new RgbColor(RgbColor.fromArgbInt(color)));
//                shapeElement.setRgbColor(new RgbColor(Color.argb(255,((i*255/6)),((i*255/7)),((i*255/8)))));
                woWoViewPagerFragment.setBackground(shapeElement);
                viewPagerTests.add(woWoViewPagerFragment);
            }
            WoWoViewPagerAdapter adapter = new WoWoViewPagerAdapter(viewPagerTests);
            adapter.setFragmentsNumber(fragmentNumber);
            if (colorRes != null) adapter.setColorRes(colorRes);
            if (color != null) adapter.setColor(color);
            if (colorsRes != null) adapter.setColorsRes(colorsRes);
            if (colors != null) adapter.setColors(colors);
            return adapter;
        }
    }
}
