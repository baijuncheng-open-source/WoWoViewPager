package com.nightonke.wowoviewpager;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * Created by Weiping Huang at 15:16 on 2017/4/1
 * For Personal Open Source
 * Contact me at 2584541288@qq.com or nightonke@outlook.com
 * For more projects: https://github.com/Nightonke
 */

public class WoWoViewPagerFragment extends Component {

    private Integer colorRes = null;
    private Integer color = null;

    public WoWoViewPagerFragment(Context context) {
        super(context);
        this.colorRes = Color.TRANSPARENT.getValue();
        onCreateView(context);
    }


    public Integer getColorRes() {
        return colorRes;
    }

    public void setColorRes(Integer colorRes) {
        this.colorRes = colorRes;
        color = null;
    }

    public Integer getColor() {
        return color;
    }

    public void setColor(Integer color) {
        this.color = color;
        colorRes = null;
    }

    public Component onCreateView(Context context) {
        Component view = new Component(getContext());
        view.setLayoutConfig(new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT));
        if (colorRes != null) {
            // the resource of color has been set
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(new RgbColor(Color.TRANSPARENT.getValue()));
            view.setBackground(shapeElement);
        } else {
            if (color != null) {
                // the color has been set
                ShapeElement shapeElement = new ShapeElement();
                shapeElement.setRgbColor(new RgbColor(color));
                view.setBackground(shapeElement);
            } else {
                // set transparent
                ShapeElement shapeElement = new ShapeElement();
                shapeElement.setRgbColor(new RgbColor(Color.TRANSPARENT.getValue()));
                view.setBackground(shapeElement);
            }
        }
        return view;
    }
}
