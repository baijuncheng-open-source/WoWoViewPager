package com.nightonke.wowoviewpager.svg;


import com.nightonke.wowoviewpager.Animation.WoWoAnimationInterface;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.render.PathEffect;
import ohos.agp.render.PathMeasure;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Color;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;
import ohos.app.Context;


/**
 * Created by Weiping Huang at 09:16 on 2017/4/1
 * For Personal Open Source
 * Contact me at 2584541288@qq.com or nightonke@outlook.com
 * For more projects: https://github.com/Nightonke
 * <p>
 * Modified from https://github.com/jrummyapps/AnimatedSvgView.
 */

public class WoWoSvgView extends Component implements WoWoAnimationInterface, Component.DrawTask, Component.EstimateSizeListener {

    private static final String TAG = "WoWoSvgView";

//    private static final Interpolator INTERPOLATOR = new DecelerateInterpolator();

    private static float constrain(float min, float max, float v) {
        return Math.max(min, Math.min(max, v));
    }

    private float mProcess = 0;
    private int mTraceTime = 2000;
    private int mTraceTimePerGlyph = 1000;
    private int mFillStart = 1200;
    private int mFillTime = 1000;
    private int[] mTraceResidueColors;
    private int[] mTraceColors;
    private float mViewportWidth;
    private float mViewportHeight;
    private Point mViewport = new Point(mViewportWidth, mViewportHeight);
    private float aspectRatioWidth = 1;
    private float aspectRatioHeight = 1;

    private Paint mFillPaint;
    private int[] mFillColors;
    private GlyphData[] mGlyphData;
    private String[] mGlyphStrings;
    private float mMarkerLength;
    private int mWidth;
    private int mHeight;

    public WoWoSvgView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(context, null);
    }

    public WoWoSvgView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context, null);
    }

    public WoWoSvgView(Context context) {
        super(context);
        init(context, null);
    }

    private void init(Context context, AttrSet attrs) {
        mFillPaint = new Paint();
        mFillPaint.setAntiAlias(true);
        mFillPaint.setStyle(Paint.Style.FILL_STYLE);

        mTraceColors = new int[1];
        mTraceColors[0] = Color.BLACK.getValue();
        mTraceResidueColors = new int[1];
        mTraceResidueColors[0] = Color.BLACK.getValue();

        if (attrs != null) {
            if (attrs.getAttr("WoWoSvgView_wowo_imageSizeX").isPresent()) {
                mViewportWidth = attrs.getAttr("WoWoSvgView_wowo_imageSizeX").get().getDimensionValue();
            } else {
                mViewportWidth = 512;
            }
            aspectRatioWidth = mViewportWidth;
            if (attrs.getAttr("WoWoSvgView_wowo_imageSizeY").isPresent()) {
                mViewportHeight = attrs.getAttr("WoWoSvgView_wowo_imageSizeY").get().getDimensionValue();
            } else {
                mViewportHeight = 512;
            }
            aspectRatioHeight = mViewportHeight;
            if (attrs.getAttr("WoWoSvgView_wowo_traceTime").isPresent()) {
                mTraceTime = attrs.getAttr("WoWoSvgView_wowo_traceTime").get().getIntegerValue();
            } else {
                mTraceTime = 2000;
            }
            if (attrs.getAttr("mTraceTimePerGlyph").isPresent()) {
                mTraceTimePerGlyph = attrs.getAttr("mTraceTimePerGlyph").get().getIntegerValue();
            } else {
                mTraceTimePerGlyph = 1000;
            }
            if (attrs.getAttr("WoWoSvgView_wowo_fillStart").isPresent()) {
                mFillStart = attrs.getAttr("WoWoSvgView_wowo_fillStart").get().getIntegerValue();
            } else {
                mFillStart = 1200;
            }
            if (attrs.getAttr("WoWoSvgView_wowo_fillTime").isPresent()) {
                mFillTime = attrs.getAttr("WoWoSvgView_wowo_fillTime").get().getIntegerValue();
            } else {
                mFillTime = 1000;
            }
            if (attrs.getAttr("WoWoSvgView_wowo_traceMarkerLength").isPresent()) {
                mMarkerLength = attrs.getAttr("WoWoSvgView_wowo_traceMarkerLength").get().getDimensionValue();
            } else {
                mMarkerLength = AttrHelper.vp2px(16, this.getContext());
            }
            int glyphStringsId;
            if (attrs.getAttr("WoWoSvgView_wowo_glyphStrings").isPresent()) {
                glyphStringsId = attrs.getAttr("WoWoSvgView_wowo_glyphStrings").get().getIntegerValue();
            } else {
                glyphStringsId = 0;
            }
            int traceResidueColorsId;
            if (attrs.getAttr("WoWoSvgView_wowo_traceResidueColors").isPresent()) {
                traceResidueColorsId = attrs.getAttr("WoWoSvgView_wowo_traceResidueColors").get().getIntegerValue();
            } else {
                traceResidueColorsId = 0;
            }
            int traceColorsId;
            if (attrs.getAttr("WoWoSvgView_wowo_traceResidueColors").isPresent()) {
                traceColorsId = attrs.getAttr("WoWoSvgView_wowo_traceResidueColors").get().getIntegerValue();
            } else {
                traceColorsId = 0;
            }
            int fillColorsId;
            if (attrs.getAttr("WoWoSvgView_wowo_traceResidueColors").isPresent()) {
                fillColorsId = attrs.getAttr("WoWoSvgView_wowo_traceResidueColors").get().getIntegerValue();
            } else {
                fillColorsId = 0;
            }


            if (glyphStringsId != 0) {
                setGlyphStrings(getContext().getString(glyphStringsId));
                setTraceResidueColor(Color.argb(50, 0, 0, 0));
                setTraceColor(Color.BLACK.getValue());
            }
            if (traceResidueColorsId != 0) {
                setTraceResidueColors(getContext().getIntArray(traceResidueColorsId));
            }
            if (traceColorsId != 0) {
                setTraceColors(getContext().getIntArray(traceColorsId));
            }
            if (fillColorsId != 0) {
                setFillColors(getContext().getIntArray(fillColorsId));
            }

            mViewport = new Point(mViewportWidth, mViewportHeight);
        }

        // Note: using a software layer here is an optimization. This view works with hardware accelerated rendering but
        // every time a path is modified (when the dash path effect is modified), the graphics pipeline will rasterize
        // the path again in a new texture. Since we are dealing with dozens of paths, it is much more efficient to
        // rasterize the entire view into a single re-usable texture instead. Ideally this should be toggled using a
        // heuristic based on the number and or dimensions of paths to render.
//            setLayerType(LAYER_TYPE_SOFTWARE, null);
//        setLayoutDirection(LayoutDirection.LTR);
        setEstimateSizeListener(this);
        addDrawTask(this);
    }

//    @Override
//    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
//        super.onSizeChanged(w, h, oldw, oldh);
//        mWidth = w;
//        mHeight = h;
//        rebuildGlyphData();
//    }


    /**
     * If you set the SVG data paths more than once using {@link #setGlyphStrings(String...)} you should call this method
     * before playing the animation.
     */
    @SuppressWarnings("SuspiciousNameCombination")
    public void rebuildGlyphData() {

        int X = mWidth / mViewport.getPointXToInt();
        int Y = mHeight / mViewport.getPointYToInt();

        Matrix scaleMatrix = new Matrix();
        Rect outerRect = new Rect(X, X, Y, Y);
        scaleMatrix.setScale(X, Y, outerRect.getCenterX(), outerRect.getCenterY());

        mGlyphData = new GlyphData[mGlyphStrings.length];
        for (int i = 0; i < mGlyphStrings.length; i++) {
            mGlyphData[i] = new GlyphData();
            try {
                mGlyphData[i].path = new Path();
                mGlyphData[i].path.transform(scaleMatrix);
                mGlyphData[i].path = WoWoPathParser.createPathFromPathData(mGlyphStrings[i]);
            } catch (Exception e) {
                mGlyphData[i].path = new Path();
//                Log.e(TAG, "Couldn't parse path", e);
            }
            PathMeasure pm = new PathMeasure(mGlyphData[i].path, true);
//            while (!pm.isClosed()) {
            mGlyphData[i].length = Math.max(mGlyphData[i].length, pm.getLength());
//                if (pm.isClosed()) {
//                    break;
//                }
//            }
            mGlyphData[i].paint = new Paint();
            mGlyphData[i].paint.setStyle(Paint.Style.STROKE_STYLE);
            mGlyphData[i].paint.setAntiAlias(true);
            mGlyphData[i].paint.setColor(Color.WHITE);
            mGlyphData[i].paint.setStrokeWidth(AttrHelper.vp2px(1, getContext())
            );
        }
    }

    /**
     * Set the viewport width and height of the SVG. This can be found in the viewBox in the SVG. This is not the size
     * of the view.
     *
     * @param viewportWidth  the width
     * @param viewportHeight the height
     */
    public void setViewportSize(float viewportWidth, float viewportHeight) {
        mViewportWidth = viewportWidth;
        mViewportHeight = viewportHeight;
        aspectRatioWidth = viewportWidth;
        aspectRatioHeight = viewportHeight;
        mViewport = new Point(mViewportWidth, mViewportHeight);
        invalidate();
    }

    /**
     * Set the SVG path data.
     *
     * @param glyphStrings The path strings found in the SVG.
     */
    public void setGlyphStrings(String... glyphStrings) {
        mGlyphStrings = glyphStrings;
    }

    /**
     * Set the colors used during tracing the SVG
     *
     * @param traceResidueColors the colors. Should be the same length as the SVG paths.
     */
    public void setTraceResidueColors(int[] traceResidueColors) {
        mTraceResidueColors = traceResidueColors;
    }

    /**
     * Set the colors used to trace the SVG.
     *
     * @param traceColors The colors. Should be the same length as the SVG paths.
     */
    public void setTraceColors(int[] traceColors) {
        mTraceColors = traceColors;
    }

    /**
     * Set the colors for the SVG. This corresponds with each data path.
     *
     * @param fillColors The colors for each SVG data path.
     */
    public void setFillColors(int[] fillColors) {
        mFillColors = fillColors;
    }

    /**
     * Set the color used for tracing. This will be applied to all data paths.
     *
     * @param color The color
     */
    public void setTraceResidueColor(int color) {
        if (mGlyphStrings == null) {
            throw new RuntimeException("You need to set the glyphs first.");
        }
        int length = mGlyphStrings.length;
        int[] colors = new int[length];
        for (int i = 0; i < length; i++) {
            colors[i] = color;
        }
        setTraceResidueColors(colors);
    }

    /**
     * Set the color used for tracing. This will be applied to all data paths.
     *
     * @param color The color
     */
    public void setTraceColor(int color) {
        if (mGlyphStrings == null) {
            throw new RuntimeException("You need to set the glyphs first.");
        }
        int length = mGlyphStrings.length;
        int[] colors = new int[length];
        for (int i = 0; i < length; i++) {
            colors[i] = color;
        }
        setTraceColors(colors);
    }

    /**
     * Set the color used for the icon. This will apply the color to all SVG data paths.
     *
     * @param color The color
     */
    public void setFillColor(int color) {
        if (mGlyphStrings == null) {
            throw new RuntimeException("You need to set the glyphs first.");
        }
        int length = mGlyphStrings.length;
        int[] colors = new int[length];
        for (int i = 0; i < length; i++) {
            colors[i] = color;
        }
        setFillColors(colors);
    }

    /**
     * Set the animation trace time
     *
     * @param traceTime time in milliseconds
     */
    public void setTraceTime(int traceTime) {
        mTraceTime = traceTime;
    }

    /**
     * Set the time used to trace each glyph
     *
     * @param traceTimePerGlyph time in milliseconds
     */
    public void setTraceTimePerGlyph(int traceTimePerGlyph) {
        mTraceTimePerGlyph = traceTimePerGlyph;
    }

    /**
     * Set the time at which colors will start being filled after the tracing begins
     *
     * @param fillStart time in milliseconds
     */
    public void setFillStart(int fillStart) {
        mFillStart = fillStart;
    }

    /**
     * Set the time it takes to fill colors
     *
     * @param fillTime time in milliseconds
     */
    public void setFillTime(int fillTime) {
        mFillTime = fillTime;
    }

    public void setProcess(float process) {
        mProcess = process;
        invalidate();
    }

    @Override
    public void toStartState() {
        setProcess(0);
    }

    @Override
    public void toMiddleState(float offset) {
        if (offset < 0) offset = 0;
        if (offset > 1) offset = 1;
        setProcess(offset);
    }

    @Override
    public void toEndState() {
        setProcess(1);
    }

    private static final class GlyphData {
        Path path;
        Paint paint;
        float length;
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        int wMode = EstimateSpec.getMode(widthMeasureSpec);
        int width = Component.EstimateSpec.getSize(widthMeasureSpec);
        int hMode = EstimateSpec.getMode(heightMeasureSpec);
        int height = Component.EstimateSpec.getSize(heightMeasureSpec);
        if (wMode == EstimateSpec.PRECISE) {
            width = EstimateSpec.getSizeWithMode(width, Component.EstimateSpec.PRECISE);
        } else if (wMode == EstimateSpec.NOT_EXCEED) {
            width = EstimateSpec.getSizeWithMode(width, Component.EstimateSpec.NOT_EXCEED);
        } else {
            width = EstimateSpec.getSizeWithMode(width, EstimateSpec.UNCONSTRAINT);
        }
        if (hMode == EstimateSpec.PRECISE) {
            height = EstimateSpec.getSizeWithMode(height, Component.EstimateSpec.PRECISE);
        } else if (hMode == EstimateSpec.NOT_EXCEED) {
            height = EstimateSpec.getSizeWithMode(height, Component.EstimateSpec.NOT_EXCEED);
        } else {
            height = EstimateSpec.getSizeWithMode(height, Component.EstimateSpec.UNCONSTRAINT);
        }

//        if (height <= 0 && width <= 0 && hMode == EstimateSpec.NOT_EXCEED &&
//                wMode ==EstimateSpec.NOT_EXCEED) {
//            width = 0;
//            height = 0;
//        } else if (height <= 0 && hMode == EstimateSpec.NOT_EXCEED ) {
//            height = (int) (width * aspectRatioHeight / aspectRatioWidth);
//        } else if (width <= 0 && wMode ==EstimateSpec.NOT_EXCEED) {
//            width = (int) (height * aspectRatioWidth / aspectRatioHeight);
//        } else if (width * aspectRatioHeight > aspectRatioWidth * height) {
//            width = (int) (height * aspectRatioWidth / aspectRatioHeight);
//        } else {
//            height = (int) (width * aspectRatioHeight / aspectRatioWidth);
//        }
        setEstimatedSize(
                width,
                height);
//        mWidth = width;
//        mHeight = height;
//        rebuildGlyphData();
        return true;
    }


    public float getInterpolation(float input) {
        int v = 1;
        if (v == 0) return input;
        else if (v > 0) return (float) (1 - Math.pow(1 - input, v));
        else return (float) Math.pow(input, -v);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (mGlyphData == null) {
            return;
        }

        long t = (long) ((Math.max(mTraceTime, mFillStart) + mFillTime) * mProcess);

        // Draw outlines (starts as traced)
        for (int i = 0; i < mGlyphData.length; i++) {
            float phase = constrain(0, 1,
                    (t - (mTraceTime - mTraceTimePerGlyph) * i * 1f / mGlyphData.length) * 1f / mTraceTimePerGlyph);
            float distance = getInterpolation(phase) * mGlyphData[i].length;
            mGlyphData[i].paint.setColor(new Color(mTraceResidueColors[i]));
            mGlyphData[i].paint.setPathEffect(new PathEffect(
                    new float[]{distance, mGlyphData[i].length}, 0));
            canvas.drawPath(mGlyphData[i].path, mGlyphData[i].paint);
            mGlyphData[i].paint.setColor(new Color(mTraceColors[i]));
            mGlyphData[i].paint.setPathEffect(new PathEffect(
                    new float[]{0, distance, phase > 0 ? mMarkerLength : 0, mGlyphData[i].length}, 0));
            canvas.drawPath(mGlyphData[i].path, mGlyphData[i].paint);
        }

        if (t > mFillStart) {
            // If after fill start, draw fill
            float phase = constrain(0, 1, (t - mFillStart) * 1f / mFillTime);
            for (int i = 0; i < mGlyphData.length; i++) {
                GlyphData glyphData = mGlyphData[i];
                int fillColor = mFillColors[i];
                int a = (int) (phase * ((float) Color.alpha(fillColor) / (float) 255) * 255);
                RgbColor rgbColor = new RgbColor(RgbColor.fromArgbInt(fillColor));
                int argb = Color.argb((a), rgbColor.getRed(), rgbColor.getGreen(), rgbColor.getBlue());
                mFillPaint.setColor(new Color(argb));
                canvas.drawPath(glyphData.path, mFillPaint);
            }
        }
    }

}
